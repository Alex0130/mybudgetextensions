﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' Allgemeine Informationen über eine Assembly werden über die folgenden 
' Attribute gesteuert. Ändern Sie diese Attributwerte, um die Informationen zu ändern,
' die einer Assembly zugeordnet sind.

' Werte der Assemblyattribute überprüfen

<Assembly: AssemblyTitle("myBudget")>
<Assembly: AssemblyDescription("Software zur Planung und Überwachung von Budget")>
<Assembly: AssemblyCompany("Dr. h.c. F. Porsche AG - Marcus Hoffmann")>
<Assembly: AssemblyProduct("myBudget")>
<Assembly: AssemblyCopyright("Copyright ©  2018")>
<Assembly: AssemblyTrademark("")>

<Assembly: ComVisible(False)>

'Die folgende GUID bestimmt die ID der Typbibliothek, wenn dieses Projekt für COM verfügbar gemacht wird.
<Assembly: Guid("3f9c8bab-af39-48ec-b908-91846aeb34ff")>

' Versionsinformationen für eine Assembly bestehen aus den folgenden vier Werten:
'
'      Hauptversion
'      Nebenversion 
'      Buildnummer
'      Revision
'
' Sie können alle Werte angeben oder die standardmäßigen Build- und Revisionsnummern 
' übernehmen, indem Sie "*" eingeben:
' <Assembly: AssemblyVersion("1.0.*")> 

'<Assembly: AssemblyVersion("3.0.0.3")>
'<Assembly: AssemblyFileVersion("3.0.0.3")>
