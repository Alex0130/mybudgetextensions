﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PaketForm
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(PaketForm))
        Me.txtBeschreibung = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ComboPaket = New System.Windows.Forms.ComboBox()
        Me.lblPaket = New System.Windows.Forms.Label()
        Me.txtPaket = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cmdSaveData = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'txtBeschreibung
        '
        Me.txtBeschreibung.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBeschreibung.Location = New System.Drawing.Point(12, 155)
        Me.txtBeschreibung.Margin = New System.Windows.Forms.Padding(4)
        Me.txtBeschreibung.Multiline = True
        Me.txtBeschreibung.Name = "txtBeschreibung"
        Me.txtBeschreibung.Size = New System.Drawing.Size(379, 69)
        Me.txtBeschreibung.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.DarkRed
        Me.Label2.Location = New System.Drawing.Point(12, 135)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(103, 16)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Beschreibung"
        '
        'ComboPaket
        '
        Me.ComboPaket.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboPaket.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboPaket.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboPaket.FormattingEnabled = True
        Me.ComboPaket.Location = New System.Drawing.Point(13, 30)
        Me.ComboPaket.Margin = New System.Windows.Forms.Padding(4)
        Me.ComboPaket.Name = "ComboPaket"
        Me.ComboPaket.Size = New System.Drawing.Size(379, 24)
        Me.ComboPaket.TabIndex = 0
        '
        'lblPaket
        '
        Me.lblPaket.AutoSize = True
        Me.lblPaket.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPaket.ForeColor = System.Drawing.Color.DarkRed
        Me.lblPaket.Location = New System.Drawing.Point(9, 10)
        Me.lblPaket.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblPaket.Name = "lblPaket"
        Me.lblPaket.Size = New System.Drawing.Size(48, 16)
        Me.lblPaket.TabIndex = 10
        Me.lblPaket.Text = "Paket"
        '
        'txtPaket
        '
        Me.txtPaket.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPaket.Location = New System.Drawing.Point(13, 95)
        Me.txtPaket.Margin = New System.Windows.Forms.Padding(4)
        Me.txtPaket.Name = "txtPaket"
        Me.txtPaket.Size = New System.Drawing.Size(379, 22)
        Me.txtPaket.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.DarkRed
        Me.Label1.Location = New System.Drawing.Point(9, 74)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(93, 16)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Paket Name"
        '
        'cmdSaveData
        '
        Me.cmdSaveData.BackColor = System.Drawing.Color.DarkRed
        Me.cmdSaveData.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cmdSaveData.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdSaveData.ForeColor = System.Drawing.Color.White
        Me.cmdSaveData.Location = New System.Drawing.Point(292, 232)
        Me.cmdSaveData.Margin = New System.Windows.Forms.Padding(4)
        Me.cmdSaveData.Name = "cmdSaveData"
        Me.cmdSaveData.Size = New System.Drawing.Size(100, 35)
        Me.cmdSaveData.TabIndex = 3
        Me.cmdSaveData.Text = "Speichern"
        Me.cmdSaveData.UseVisualStyleBackColor = False
        '
        'PaketForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(406, 279)
        Me.Controls.Add(Me.cmdSaveData)
        Me.Controls.Add(Me.txtBeschreibung)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.ComboPaket)
        Me.Controls.Add(Me.lblPaket)
        Me.Controls.Add(Me.txtPaket)
        Me.Controls.Add(Me.Label1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "PaketForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "PaketForm"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents txtBeschreibung As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents ComboPaket As System.Windows.Forms.ComboBox
    Friend WithEvents lblPaket As System.Windows.Forms.Label
    Friend WithEvents txtPaket As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cmdSaveData As System.Windows.Forms.Button
End Class
