﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ProjektForm
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ProjektForm))
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ComboProjekt = New System.Windows.Forms.ComboBox()
        Me.lblProjekt = New System.Windows.Forms.Label()
        Me.txtProjekt = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cmdSaveData = New System.Windows.Forms.Button()
        Me.DateTimeProjektstart = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtProjektlaufzeit = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.DarkRed
        Me.Label2.Location = New System.Drawing.Point(10, 126)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(87, 16)
        Me.Label2.TabIndex = 32
        Me.Label2.Text = "Projektstart"
        '
        'ComboProjekt
        '
        Me.ComboProjekt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboProjekt.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboProjekt.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboProjekt.FormattingEnabled = True
        Me.ComboProjekt.Location = New System.Drawing.Point(13, 27)
        Me.ComboProjekt.Margin = New System.Windows.Forms.Padding(4)
        Me.ComboProjekt.Name = "ComboProjekt"
        Me.ComboProjekt.Size = New System.Drawing.Size(379, 24)
        Me.ComboProjekt.TabIndex = 0
        '
        'lblProjekt
        '
        Me.lblProjekt.AutoSize = True
        Me.lblProjekt.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProjekt.ForeColor = System.Drawing.Color.DarkRed
        Me.lblProjekt.Location = New System.Drawing.Point(9, 7)
        Me.lblProjekt.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblProjekt.Name = "lblProjekt"
        Me.lblProjekt.Size = New System.Drawing.Size(57, 16)
        Me.lblProjekt.TabIndex = 30
        Me.lblProjekt.Text = "Projekt"
        '
        'txtProjekt
        '
        Me.txtProjekt.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtProjekt.Location = New System.Drawing.Point(13, 92)
        Me.txtProjekt.Margin = New System.Windows.Forms.Padding(4)
        Me.txtProjekt.Name = "txtProjekt"
        Me.txtProjekt.Size = New System.Drawing.Size(379, 22)
        Me.txtProjekt.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.DarkRed
        Me.Label1.Location = New System.Drawing.Point(9, 71)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(102, 16)
        Me.Label1.TabIndex = 27
        Me.Label1.Text = "Projekt Name"
        '
        'cmdSaveData
        '
        Me.cmdSaveData.BackColor = System.Drawing.Color.DarkRed
        Me.cmdSaveData.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cmdSaveData.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdSaveData.ForeColor = System.Drawing.Color.White
        Me.cmdSaveData.Location = New System.Drawing.Point(289, 223)
        Me.cmdSaveData.Margin = New System.Windows.Forms.Padding(4)
        Me.cmdSaveData.Name = "cmdSaveData"
        Me.cmdSaveData.Size = New System.Drawing.Size(100, 35)
        Me.cmdSaveData.TabIndex = 4
        Me.cmdSaveData.Text = "Speichern"
        Me.cmdSaveData.UseVisualStyleBackColor = False
        '
        'DateTimeProjektstart
        '
        Me.DateTimeProjektstart.Location = New System.Drawing.Point(12, 146)
        Me.DateTimeProjektstart.Name = "DateTimeProjektstart"
        Me.DateTimeProjektstart.Size = New System.Drawing.Size(200, 20)
        Me.DateTimeProjektstart.TabIndex = 2
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.DarkRed
        Me.Label3.Location = New System.Drawing.Point(13, 179)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(173, 16)
        Me.Label3.TabIndex = 37
        Me.Label3.Text = "Projektlaufzeit in Jahren"
        '
        'txtProjektlaufzeit
        '
        Me.txtProjektlaufzeit.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtProjektlaufzeit.Location = New System.Drawing.Point(13, 199)
        Me.txtProjektlaufzeit.Margin = New System.Windows.Forms.Padding(4)
        Me.txtProjektlaufzeit.Name = "txtProjektlaufzeit"
        Me.txtProjektlaufzeit.Size = New System.Drawing.Size(84, 22)
        Me.txtProjektlaufzeit.TabIndex = 3
        '
        'ProjektForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(402, 270)
        Me.Controls.Add(Me.txtProjektlaufzeit)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.DateTimeProjektstart)
        Me.Controls.Add(Me.cmdSaveData)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.ComboProjekt)
        Me.Controls.Add(Me.lblProjekt)
        Me.Controls.Add(Me.txtProjekt)
        Me.Controls.Add(Me.Label1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "ProjektForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Projekt"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents ComboProjekt As System.Windows.Forms.ComboBox
    Friend WithEvents lblProjekt As System.Windows.Forms.Label
    Friend WithEvents txtProjekt As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cmdSaveData As System.Windows.Forms.Button
    Friend WithEvents DateTimeProjektstart As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtProjektlaufzeit As System.Windows.Forms.TextBox
End Class
