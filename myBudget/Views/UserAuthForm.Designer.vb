﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UserAuthForm
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(UserAuthForm))
        Me.Grid = New TenTec.Windows.iGridLib.iGrid()
        Me.IGrid1DefaultCellStyle = New TenTec.Windows.iGridLib.iGCellStyle(True)
        Me.IGrid1DefaultColHdrStyle = New TenTec.Windows.iGridLib.iGColHdrStyle(True)
        Me.IGrid1RowTextColCellStyle = New TenTec.Windows.iGridLib.iGCellStyle(True)
        Me.ComboUser = New System.Windows.Forms.ComboBox()
        Me.GridDropDownRW = New TenTec.Windows.iGridLib.iGDropDownList()
        Me.IGAutoFilterManager1 = New TenTec.Windows.iGridLib.Filtering.iGAutoFilterManager()
        Me.cmdAll = New System.Windows.Forms.Button()
        Me.cmdNone = New System.Windows.Forms.Button()
        Me.cmdSave = New System.Windows.Forms.Button()
        CType(Me.Grid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Grid
        '
        Me.Grid.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Grid.DefaultCol.CellStyle = Me.IGrid1DefaultCellStyle
        Me.Grid.DefaultCol.ColHdrStyle = Me.IGrid1DefaultColHdrStyle
        Me.Grid.Location = New System.Drawing.Point(12, 39)
        Me.Grid.Name = "Grid"
        Me.Grid.Size = New System.Drawing.Size(776, 399)
        Me.Grid.TabIndex = 0
        '
        'ComboUser
        '
        Me.ComboUser.FormattingEnabled = True
        Me.ComboUser.Location = New System.Drawing.Point(12, 12)
        Me.ComboUser.Name = "ComboUser"
        Me.ComboUser.Size = New System.Drawing.Size(227, 21)
        Me.ComboUser.TabIndex = 1
        '
        'IGAutoFilterManager1
        '
        Me.IGAutoFilterManager1.Grid = Me.Grid
        '
        'cmdAll
        '
        Me.cmdAll.Location = New System.Drawing.Point(276, 12)
        Me.cmdAll.Name = "cmdAll"
        Me.cmdAll.Size = New System.Drawing.Size(75, 23)
        Me.cmdAll.TabIndex = 2
        Me.cmdAll.Text = "Alle"
        Me.cmdAll.UseVisualStyleBackColor = True
        '
        'cmdNone
        '
        Me.cmdNone.Location = New System.Drawing.Point(357, 12)
        Me.cmdNone.Name = "cmdNone"
        Me.cmdNone.Size = New System.Drawing.Size(75, 23)
        Me.cmdNone.TabIndex = 3
        Me.cmdNone.Text = "Keine"
        Me.cmdNone.UseVisualStyleBackColor = True
        '
        'cmdSave
        '
        Me.cmdSave.Location = New System.Drawing.Point(713, 12)
        Me.cmdSave.Name = "cmdSave"
        Me.cmdSave.Size = New System.Drawing.Size(75, 23)
        Me.cmdSave.TabIndex = 4
        Me.cmdSave.Text = "Speichern"
        Me.cmdSave.UseVisualStyleBackColor = True
        '
        'UserAuthForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.cmdSave)
        Me.Controls.Add(Me.cmdNone)
        Me.Controls.Add(Me.cmdAll)
        Me.Controls.Add(Me.ComboUser)
        Me.Controls.Add(Me.Grid)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "UserAuthForm"
        Me.Text = "User Authorisierung"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.Grid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Grid As TenTec.Windows.iGridLib.iGrid
    Friend WithEvents IGrid1DefaultCellStyle As TenTec.Windows.iGridLib.iGCellStyle
    Friend WithEvents IGrid1DefaultColHdrStyle As TenTec.Windows.iGridLib.iGColHdrStyle
    Friend WithEvents IGrid1RowTextColCellStyle As TenTec.Windows.iGridLib.iGCellStyle
    Friend WithEvents ComboUser As ComboBox
    Friend WithEvents GridDropDownRW As TenTec.Windows.iGridLib.iGDropDownList
    Friend WithEvents IGAutoFilterManager1 As TenTec.Windows.iGridLib.Filtering.iGAutoFilterManager
    Friend WithEvents cmdAll As System.Windows.Forms.Button
    Friend WithEvents cmdNone As System.Windows.Forms.Button
    Friend WithEvents cmdSave As System.Windows.Forms.Button
End Class
