﻿Public Class UserAuthForm
#Region "Events"
    Private Sub UserAuthForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Load_Form()
    End Sub

    Private Sub ComboUser_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboUser.SelectedIndexChanged
        SetUserAuth(ComboUser.SelectedIndex)
    End Sub

    Private Sub cmdAll_Click(sender As Object, e As EventArgs) Handles cmdAll.Click
        Dim I As Integer
        For I = 0 To Grid.Rows.Count - 1
            If Grid.Rows(I).Visible = True Then
                Grid.Rows(I).Cells("Auth").Value = True
            End If
        Next
    End Sub

    Private Sub cmdNone_Click(sender As Object, e As EventArgs) Handles cmdNone.Click
        Dim I As Integer
        For I = 0 To Grid.Rows.Count - 1
            If Grid.Rows(I).Visible = True Then
                Grid.Rows(I).Cells("Auth").Value = False
            End If
        Next
    End Sub

    Private Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click
        UpdateUserAuth()
    End Sub
#End Region

#Region "Methoden"
    Public Sub Load_Form()
        Dim i As Integer, j As Integer
        Try
#Region "User"
            ComboUser.Items.Clear()
            If pbl_dtUser.Rows.Count > 0 Then
                For i = 0 To pbl_dtUser.Rows.Count - 1
                    ComboUser.Items.Add(pbl_dtUser.Rows(i).Item("txtName") & ", " & pbl_dtUser.Rows(i).Item("txtVorname") & " - " & pbl_dtUser.Rows(i).Item("txtPNummer"))
                Next
            End If
#End Region

#Region "Arbeitspakete"
            Dim dt As New System.Data.DataTable
            Dim dtProject As New System.Data.DataTable
            Dim curCol As Integer

            dt = Get_tblAlleArbeitspakete(pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer"), pbl_dtAuth, True)
            dtProject = Get_tblAlleProjekte(pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer"), pbl_dtAuth, False)

            Grid.Cols.Clear()
            Grid.Rows.Clear()

            GridDropDownRW.Items.Clear()
            GridDropDownRW.Items.Add("Write")
            GridDropDownRW.Items.Add("Read")

            If dt.Columns.Count > 0 Then
                For i = 0 To dt.Columns.Count - 1
                    Grid.Cols.Add()
                    curCol = Grid.Cols.Count - 1
                    'Define column
                    Grid.Cols(curCol).Key = dt.Columns(i).ColumnName
                    Grid.Cols(curCol).Text = dt.Columns(i).ColumnName
                    Grid.Cols(curCol).CellStyle.ValueType = dt.Columns(i).DataType
                Next
            End If

#Region "Additional Columns"
            Grid.Cols.Add("Berechtigung")
            Grid.Cols(Grid.Cols.Count - 1).Key = "Auth"
            Grid.Cols(Grid.Cols.Count - 1).CellStyle.Type = TenTec.Windows.iGridLib.iGCellType.Check
            Grid.Cols(Grid.Cols.Count - 1).CellStyle.ImageAlign = TenTec.Windows.iGridLib.iGContentAlignment.TopCenter

            Grid.Cols.Add("ReadWrite").DefaultCellValue = "Write"
            Grid.Cols(Grid.Cols.Count - 1).Key = "RW"
            Grid.Cols(Grid.Cols.Count - 1).CellStyle.ValueType = GetType(System.String)
            Grid.Cols("RW").CellStyle.DropDownControl = GridDropDownRW

            Grid.Cols.Add("Projekt")
            Grid.Cols(Grid.Cols.Count - 1).Key = "Projekt"
            Grid.Cols(Grid.Cols.Count - 1).CellStyle.ValueType = GetType(System.String)
#End Region

            Grid.FillWithData(dt, True)

            Dim curProjekt As String, curProjektId As String
            If Grid.Cols.Count > 0 Then
                For i = 0 To Grid.Rows.Count - 1
                    If Not Grid.Rows(i).Cells("qryProjekt").Value = curProjektId Then
                        Dim foundRows() As Data.DataRow
                        foundRows = dtProject.Select("ID_PROJEKT = '" & Grid.Rows(i).Cells("qryProjekt").Value & "'")
                        If Not foundRows.Count = 0 Then
                            curProjekt = foundRows(0).Item("txtProjekt")
                            curProjektId = foundRows(0).Item("ID_PROJEKT")
                        Else
                            curProjekt = ""
                            curProjektId = 0
                        End If
                    End If
                    Grid.Rows(i).Cells("Projekt").Value = curProjekt
                Next
            End If


            Grid.Cols.AutoWidth()
            Grid.Rows.AutoHeight()
#End Region
        Catch ex As Exception
            MsgBox("Load_Form: " & ex.Message.ToString)
        End Try
    End Sub

    Public Sub SetUserAuth(UserIndex As Integer)
        Try
            Dim i As Integer, j As Integer

            '1. User Auth herunterladen
            '2. Alte Werte zurücksetzen
            '3. Rollen mit Daten vergleichen
            Dim dtAuth As System.Data.DataTable
            dtAuth = Get_tblAuthorization(False, pbl_dtUser.Rows(UserIndex).Item("txtPNummer"))

            If dtAuth.Rows.Count > 0 AndAlso Grid.Rows.Count > 0 Then

                For j = 0 To Grid.Rows.Count - 1
                    Grid.Rows(j).Cells("RW").Value = "Write"
                    Grid.Rows(j).Cells("Auth").Value = False

                    Dim foundRows() As Data.DataRow
                    dtAuth.CaseSensitive = False 'Groß und Kleinschreibung ignorieren
                    foundRows = dtAuth.Select("txtTransaktion = 'ID_AP' and intID = " & Grid.Rows(j).Cells("ID_AP").Value & "")
                    If foundRows.Count > 0 Then
                        For i = 0 To foundRows.Count - 1
                            Grid.Rows(j).Cells("RW").Value = foundRows(i).Item("ReadWrite")
                            Grid.Rows(j).Cells("Auth").Value = True
                        Next i
                    End If
                Next
            End If

        Catch ex As Exception
            MsgBox("SetUserAuthex: " & ex.Message.ToString)
        End Try
    End Sub

    Public Sub UpdateUserAuth()
        '1. alte Berechtigungen löschen
        '2. Neue Berechtigungen setzen
        Dim i As Integer, j As Integer
        Try

            If ComboUser.SelectedIndex = 0 Or Grid.Rows.Count = 0 Then
                Exit Sub
            End If

            If InStr(UCase(pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer")), "ADMIN") > 0 Then 'Alle Berechtigungen loeschen
                Delete_UserTransaktion(pbl_dtUser.Rows(ComboUser.SelectedIndex).Item("txtPNummer"), "ID_AP")
                Delete_UserTransaktion(pbl_dtUser.Rows(ComboUser.SelectedIndex).Item("txtPNummer"), "ID_PROJEKT")
            Else 'nur Rollen fuer den User sichtbare Projekte und Arbeitspakete loeschen
                For i = 0 To Grid.Rows.Count - 1
                    Delete_UserEntry(pbl_dtUser.Rows(ComboUser.SelectedIndex).Item("txtPNummer"), "ID_AP", Grid.Rows(i).Cells("ID_AP").Value)
                    Delete_UserEntry(pbl_dtUser.Rows(ComboUser.SelectedIndex).Item("txtPNummer"), "ID_PROJEKT", Grid.Rows(i).Cells("qryProjekt").Value)
                Next
            End If


            Dim Projects() As String, BoolGotProject As Boolean
            Dim boolSaved As Boolean
            For i = 0 To Grid.Rows.Count - 1
                If Grid.Rows(i).Cells("Auth").Value = True Then
                    BoolGotProject = False
                    Add_Authorization("ID_AP", Grid.Rows(i).Cells("ID_AP").Value, pbl_dtUser.Rows(ComboUser.SelectedIndex).Item("txtPNummer"), Grid.Rows(i).Cells("RW").Value)

#Region "Zugehörige Projekte speichern"
                    If IsNothing(Projects) Then
                        ReDim Projects(0)
                        Projects(0) = Grid.Rows(i).Cells("qryProjekt").Value
                    Else
                        For j = 0 To UBound(Projects)
                            If Projects(j) = Grid.Rows(i).Cells("qryProjekt").Value Then
                                BoolGotProject = True
                                Exit For
                            End If
                        Next
                        If BoolGotProject = False Then
                            ReDim Preserve Projects(UBound(Projects) + 1)
                            Projects(UBound(Projects)) = Grid.Rows(i).Cells("qryProjekt").Value
                        End If
                    End If
#End Region

                End If
            Next

            If Not IsNothing(Projects) Then
                For i = 0 To UBound(Projects)
                    Add_Authorization("ID_PROJEKT", Projects(i), pbl_dtUser.Rows(ComboUser.SelectedIndex).Item("txtPNummer"), "Write")
                Next
            End If

            MsgBox("User Authorisierung erfolgreich geändert!", vbInformation, "Speichern erfolgreich")

        Catch ex As Exception
            MsgBox("UpdateUserAuth: " & ex.Message.ToString)
        End Try
    End Sub
#End Region
End Class