﻿Imports TenTec.Windows.iGridLib

Public Class DatenabfrageForm

    Private fMSDDE1 As New iGMultiSelectDropDownEditor
    Public pbl_GetDataValue As String
#Region "Events"
    Private Sub DatenabfrageForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        'With fMSDDE1.ChoiceList
        '    .Add("Item1")
        '    .Add("Item2")
        '    .Add("Item3")
        '    .Add("Item4")
        '    .Add("Item5")
        'End With

        '' Populate the sample grid
        'With aGrid
        '    .BeginUpdate()

        '    .Cols.Count = 1
        '    .Rows.Count = 5

        '    .Cols(0).CellStyle.DropDownControl = fMSDDE1
        '    .Cols(0).CellStyle.TypeFlags = iGCellTypeFlags.NoTextEdit
        '    .Cols(0).Width = 180

        '    .Cells(0, 0).Value = "Item1"
        '    .Cells(1, 0).Value = "Item2, Item4"
        '    .Cells(4, 0).Value = "Item1, Item2, Item3, Item4, Item5"

        '    ' Select the first cell to test the keyboard interface:
        '    ' F2 opens the multi-select drop-down editor; then you can
        '    ' use the Spacebar, Enter and Escape keys in the drop-down form.
        '    .SetCurCell(0, 0)

        '    .EndUpdate()
        'End With

        Form_Load()

    End Sub

#End Region

#Region "Methoden"
    Sub Form_Load()
        Dim i As Integer, j As Integer

        Try
            RemoveHandler aGrid.AfterCommitEdit, AddressOf aGrid_AfterCommitEdit

#Region "iGrid Daten"
            Dim dt As New System.Data.DataTable
            'Dim dtProject As New System.Data.DataTable

#Region "Datenstruktur von tblData abfragen"
            dt = Get_tblDataStructure()
#End Region
            'dtProject = Get_tblAlleProjekte(pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer"), pbl_Auth)

            aGrid.Cols.Clear()
            aGrid.Rows.Clear()

            DropDowntblDataStruktur.Items.Clear()
            If dt.Rows.Count > 0 Then
                For i = 0 To dt.Rows.Count - 1
                    DropDowntblDataStruktur.Items.Add(dt.Rows(i).Item("ColumnName"))
                Next i
            End If

#Region "Additional Columns"
            With aGrid
                .Cols.Add("Feld")
                .Cols(aGrid.Cols.Count - 1).Key = "Feld"
                .Cols(aGrid.Cols.Count - 1).CellStyle.ValueType = GetType(System.String)
                .Cols("Feld").CellStyle.DropDownControl = DropDowntblDataStruktur


                .Cols.Add("Auswahl")
                .Cols(aGrid.Cols.Count - 1).Key = "Auswahl"
                .Cols(aGrid.Cols.Count - 1).CellStyle.DropDownControl = fMSDDE1
                .Cols(aGrid.Cols.Count - 1).CellStyle.TypeFlags = iGCellTypeFlags.NoTextEdit
                .Cols(aGrid.Cols.Count - 1).Width = 180
            End With
#End Region
            aGrid.Rows.Add()
            aGrid.Rows(0).Cells("Feld").Value = "ID_DATA"
            '            Grid.FillWithData(dt, True)

            '            Dim curProjekt As String, curProjektId As String
            '            If Grid.Cols.Count > 0 Then
            '                For i = 0 To Grid.Rows.Count - 1
            '                    If Not Grid.Rows(i).Cells("qryProjekt").Value = curProjektId Then
            '                        Dim foundRows() As Data.DataRow
            '                        foundRows = dtProject.Select("ID_PROJEKT = '" & Grid.Rows(i).Cells("qryProjekt").Value & "'")
            '                        If Not foundRows.Count = 0 Then
            '                            curProjekt = foundRows(0).Item("txtProjekt")
            '                            curProjektId = foundRows(0).Item("ID_PROJEKT")
            '                        Else
            '                            curProjekt = ""
            '                            curProjektId = 0
            '                        End If
            '                    End If
            '                    Grid.Rows(i).Cells("Projekt").Value = curProjekt
            '                Next
            '            End If


            aGrid.Cols.AutoWidth()
            aGrid.Rows.AutoHeight()
#End Region
        Catch ex As Exception
            MsgBox("Load_Form: " & ex.Message.ToString)
        End Try

        AddHandler aGrid.AfterCommitEdit, AddressOf aGrid_AfterCommitEdit
    End Sub

    Public Function CreateTblDataWhereSqlStatement(FromRow As Integer, ToRow As Integer) As String
        'Beispiel: where (qryProjekt = 1 or qryProjekt = 2) and (ID_AP = 1 or ID_AP = 2 or ID_AP = 32 or ID_AP = 70)

        Dim i As Integer
        Dim sql As String
        Dim SqlAndOr() As String

        If aGrid.Rows.Count = 0 Then
            sql = ""
        ElseIf aGrid.Rows.Count = 1 And String.IsNullOrEmpty(aGrid.Rows(0).Cells("Auswahl").Value) Then
            sql = ""
        Else
            sql = " where"
            For i = 0 To aGrid.Rows.Count - 1
                If Not String.IsNullOrEmpty(aGrid.Rows(i).Cells("Feld").Value) AndAlso Not String.IsNullOrEmpty(aGrid.Rows(i).Cells("Auswahl").Value) Then

                End If
            Next
        End If

        Return sql
    End Function

    Private Sub aGrid_AfterCommitEdit(sender As Object, e As iGAfterCommitEditEventArgs) Handles aGrid.AfterCommitEdit
        'Neuer Wert der geänderten Zeilen
        Try
            If Not pbl_GetDataValue = aGrid.Rows(e.RowIndex).Cells(e.ColIndex).Value Then 'Wert hat sich geändert
                If aGrid.Rows.Count = 0 Then Exit Sub

                Dim sql As String
                Dim dt As New System.Data.DataTable
                Dim i As Integer, j As Integer

                If e.ColIndex = 0 Then 'Feld geändert: alles nachfolgende neu laden
                    For i = e.RowIndex To aGrid.Rows.Count - 1 'Alle Auswahlfelder ab hier neu laden mit den SQL Werten der vorherigen Feld/Auswahl Kombination
                        sql = CreateTblDataWhereSqlStatement(0, e.RowIndex - 1)
                        dt = Get_tblDataViaSql(aGrid.Rows(e.RowIndex).Cells(0).Value, sql)
                        If dt.Rows.Count > 0 Then
                            For j = 0 To dt.Rows.Count - 1
                                fMSDDE1.ChoiceList.Add(dt.Rows(j).Item(0))
                                'With fMSDDE1.ChoiceList
                                '    .Add("Item1")
                                '    .Add("Item2")
                                '    .Add("Item3")
                                '    .Add("Item4")
                                '    .Add("Item5")
                                'End With
                            Next
                            With aGrid
                                For j = 0 To dt.Rows.Count - 1
                                    .Cells(i, 1).Value = "a, b, c"
                                Next j
                            End With
                        End If
                    Next
                End If

            End If


        Catch ex As Exception

        End Try
    End Sub

    Private Sub aGrid_BeforeCommitEdit(sender As Object, e As iGBeforeCommitEditEventArgs) Handles aGrid.BeforeCommitEdit
        'Speichert den aktuellen Wert der geänderten Zeile
        pbl_GetDataValue = aGrid.Rows(e.RowIndex).Cells(e.ColIndex).Value
    End Sub


#End Region

End Class