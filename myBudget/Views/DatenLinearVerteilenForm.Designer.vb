﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DatenLinearVerteilenForm
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(DatenLinearVerteilenForm))
        Me.cmdSave = New System.Windows.Forms.Button()
        Me.comboVonMonat = New System.Windows.Forms.ComboBox()
        Me.comboBisMonat = New System.Windows.Forms.ComboBox()
        Me.lblArbeitspaket = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'cmdSave
        '
        Me.cmdSave.BackColor = System.Drawing.Color.DarkRed
        Me.cmdSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cmdSave.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdSave.ForeColor = System.Drawing.Color.White
        Me.cmdSave.Location = New System.Drawing.Point(191, 58)
        Me.cmdSave.Name = "cmdSave"
        Me.cmdSave.Size = New System.Drawing.Size(104, 38)
        Me.cmdSave.TabIndex = 0
        Me.cmdSave.Text = "Ausführen"
        Me.cmdSave.UseVisualStyleBackColor = False
        '
        'comboVonMonat
        '
        Me.comboVonMonat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.comboVonMonat.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.comboVonMonat.Location = New System.Drawing.Point(16, 28)
        Me.comboVonMonat.Name = "comboVonMonat"
        Me.comboVonMonat.Size = New System.Drawing.Size(121, 24)
        Me.comboVonMonat.TabIndex = 1
        '
        'comboBisMonat
        '
        Me.comboBisMonat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.comboBisMonat.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.comboBisMonat.FormattingEnabled = True
        Me.comboBisMonat.Location = New System.Drawing.Point(174, 28)
        Me.comboBisMonat.Name = "comboBisMonat"
        Me.comboBisMonat.Size = New System.Drawing.Size(121, 24)
        Me.comboBisMonat.TabIndex = 2
        '
        'lblArbeitspaket
        '
        Me.lblArbeitspaket.AutoSize = True
        Me.lblArbeitspaket.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblArbeitspaket.ForeColor = System.Drawing.Color.DarkRed
        Me.lblArbeitspaket.Location = New System.Drawing.Point(13, 9)
        Me.lblArbeitspaket.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblArbeitspaket.Name = "lblArbeitspaket"
        Me.lblArbeitspaket.Size = New System.Drawing.Size(40, 16)
        Me.lblArbeitspaket.TabIndex = 3
        Me.lblArbeitspaket.Text = "Start"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.DarkRed
        Me.Label1.Location = New System.Drawing.Point(171, 9)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(44, 16)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Ende"
        '
        'DatenLinearVerteilenForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(305, 106)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblArbeitspaket)
        Me.Controls.Add(Me.comboBisMonat)
        Me.Controls.Add(Me.comboVonMonat)
        Me.Controls.Add(Me.cmdSave)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "DatenLinearVerteilenForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Anzahl linear verteilen"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents cmdSave As System.Windows.Forms.Button
    Friend WithEvents comboVonMonat As ComboBox
    Friend WithEvents comboBisMonat As ComboBox
    Friend WithEvents lblArbeitspaket As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class
