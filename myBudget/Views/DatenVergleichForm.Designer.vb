﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DatenVergleichForm
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(DatenVergleichForm))
        Me.Label6 = New System.Windows.Forms.Label()
        Me.ComboEntwurf = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.ComboVersion = New System.Windows.Forms.ComboBox()
        Me.SaveData = New System.Windows.Forms.PictureBox()
        CType(Me.SaveData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.DarkRed
        Me.Label6.Location = New System.Drawing.Point(15, 27)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(86, 16)
        Me.Label6.TabIndex = 17
        Me.Label6.Text = "Entwurfstyp"
        '
        'ComboEntwurf
        '
        Me.ComboEntwurf.BackColor = System.Drawing.Color.White
        Me.ComboEntwurf.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboEntwurf.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboEntwurf.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboEntwurf.ForeColor = System.Drawing.Color.Black
        Me.ComboEntwurf.FormattingEnabled = True
        Me.ComboEntwurf.Location = New System.Drawing.Point(107, 24)
        Me.ComboEntwurf.Name = "ComboEntwurf"
        Me.ComboEntwurf.Size = New System.Drawing.Size(152, 24)
        Me.ComboEntwurf.TabIndex = 14
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.DarkRed
        Me.Label3.Location = New System.Drawing.Point(40, 70)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(61, 16)
        Me.Label3.TabIndex = 16
        Me.Label3.Text = "Version"
        '
        'ComboVersion
        '
        Me.ComboVersion.BackColor = System.Drawing.Color.White
        Me.ComboVersion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboVersion.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboVersion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboVersion.ForeColor = System.Drawing.Color.Black
        Me.ComboVersion.FormattingEnabled = True
        Me.ComboVersion.Location = New System.Drawing.Point(107, 67)
        Me.ComboVersion.Name = "ComboVersion"
        Me.ComboVersion.Size = New System.Drawing.Size(152, 24)
        Me.ComboVersion.TabIndex = 15
        '
        'SaveData
        '
        Me.SaveData.Image = CType(resources.GetObject("SaveData.Image"), System.Drawing.Image)
        Me.SaveData.Location = New System.Drawing.Point(224, 108)
        Me.SaveData.Name = "SaveData"
        Me.SaveData.Size = New System.Drawing.Size(35, 35)
        Me.SaveData.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.SaveData.TabIndex = 38
        Me.SaveData.TabStop = False
        '
        'DatenVergleichForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(284, 172)
        Me.Controls.Add(Me.SaveData)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.ComboEntwurf)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.ComboVersion)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "DatenVergleichForm"
        Me.Text = "Vergleiche Daten mit:"
        CType(Me.SaveData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents ComboEntwurf As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents ComboVersion As System.Windows.Forms.ComboBox
    Friend WithEvents SaveData As System.Windows.Forms.PictureBox
End Class
