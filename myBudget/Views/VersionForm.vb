﻿
Public Class VersionForm

    Public Sub Form_Version(ByVal sender As System.Object, ByVal e As _
   System.EventArgs) Handles MyBase.Load
        Console.WriteLine("EVENT: Public Sub Form_Version")

        ComboVersion.Items.Clear()
        txtErläuterung.Text = ""

        If intArt_der_Editierung = 0 Then 'NEU
            cmdSaveData.Visible = True
            ComboVersion.Visible = False
            lblVersion.Visible = False
            txtErläuterung.Enabled = True
        ElseIf intArt_der_Editierung = 1 Then 'ÄNDERUNG
            cmdSaveData.Visible = True
            ComboVersion.Visible = True
            lblVersion.Visible = True
            txtErläuterung.Enabled = True
        Else 'VIEW
            cmdSaveData.Visible = False
            ComboVersion.Visible = False
            lblVersion.Visible = False
            txtErläuterung.Enabled = False
        End If

        cbxCopyData.Checked = True
        cbxFreezeVersion.Checked = True

        Try
            ViewCombo.View_ComboboxVersion(ComboVersion, Array.FindIndex(pbl_VersionAp, Function(f) f.lngVersion = pbl_lngVersion), pbl_VersionAp)
            txtErläuterung.Text = pbl_VersionAp(ComboVersion.SelectedIndex).txtVersionErklärung
        Catch
            Try
                ViewCombo.View_ComboboxVersion(ComboVersion, 0, pbl_VersionAp)
                txtErläuterung.Text = pbl_VersionAp(ComboVersion.SelectedIndex).txtVersionErklärung
            Catch ex As Exception
                MsgBox(ex.Message.ToString, vbCritical, "Fehler")
            End Try
        End Try

    End Sub

    Private Sub cmdSaveData_Click(sender As Object, e As EventArgs) Handles cmdSaveData.Click
        Console.WriteLine("AKTION: Private Sub cmdSaveData_Click")

        boolError = False
        Dim i As Integer
        Try
            'Prüfen ob eine aktive Verbindung zum Server existiert
            If con.State = ConnectionState.Closed Or boolConnected = False Then
                MsgBox("Die Verbindung zum Server wurde unterbrochen." & vbCrLf & "Bitte verbinden Sie sich neu mit dem Server.", Title:="Fehler bei der Verbindung zum Server", Buttons:=MsgBoxStyle.Critical)
                Exit Sub
            End If

            If txtErläuterung.Text = "" Then
                MsgBox("Bitte geben Sie eine Beschreibung ein", MsgBoxStyle.Critical, "Fehler")
                Exit Sub   '!ab added
            End If

            If intArt_der_Editierung = 1 Then 'Änderung
                Try
                    Update_VersionArbeitspaket(pbl_IdAp, pbl_VersionAp(ComboVersion.SelectedIndex).lngVersion, pbl_Entwurf(pbl_IndexEntwurf).txtEntwurfstyp, pbl_VersionAp(ComboVersion.SelectedIndex).cbxFreigabe, txtErläuterung.Text)
                    Get_VersionArbeitspaket(True, pbl_IdAp, pbl_Entwurf(pbl_IndexEntwurf).txtEntwurfstyp)
                Catch ex As Exception
                    boolError = True
                    MsgBox(ex.Message.ToString, vbCritical, "Fehler")
                End Try
            Else 'NEU
                '1. Letzte Version ermitteln
                '2. Neue Version erstellen
                Try
                    Dim NewVersion As Integer = Get_LastestApVersion(pbl_IdAp, pbl_Entwurf(pbl_IndexEntwurf).txtEntwurfstyp) + 1
                    Dim OldVersion As Integer = NewVersion - 1
                    Add_VersionArbeitspaket(pbl_IdAp, NewVersion, txtErläuterung.Text, pbl_Entwurf(pbl_IndexEntwurf).txtEntwurfstyp)
                    Get_VersionArbeitspaket(True, pbl_IdAp, pbl_Entwurf(pbl_IndexEntwurf).txtEntwurfstyp)

#Region "Daten der Vorgängerversion übernehmen"
                    Try
                        If cbxCopyData.Checked = True Then
                            If Not OldVersion = 0 Then
                                Dim dtOld As New System.Data.DataTable
                                Dim sql As String = " where qryArbeitspaket = " & pbl_IdAp & " and qryVersion = " & OldVersion & " and qryEntwurfstyp = '" & pbl_Entwurf(pbl_IndexEntwurf).txtEntwurfstyp & "'"
                                dtOld = Get_tblDataViaSql("*", sql)

                                If dtOld.Rows.Count > 0 Then 'Daten zum Kopieren vorhanden
                                    For i = 0 To dtOld.Rows.Count - 1
                                        Add_Data(dtOld.Rows(i).Item("qryEntwurfstyp"),
                                    dtOld.Rows(i).Item("qryArbeitspaket"),
                                    NewVersion,
                                    dtOld.Rows(i).Item("qryGeschäftsjahr"),
                                    dtOld.Rows(i).Item("qryKostenart"),
                                    dtOld.Rows(i).Item("txtBeschreibung"),
                                    dtOld.Rows(i).Item("txtBemerkung"),
                                    dtOld.Rows(i).Item("txtZusatzfeld"),
                                    dtOld.Rows(i).Item("curKosten_pro_Einheit"),
                                    dtOld.Rows(i).Item("boolRenneinsatz"),
                                    Now,
                                    dtOld.Rows(i).Item("txtBedarfsnummer"),
                                    dtOld.Rows(i).Item("dblJanuar"),
                                    dtOld.Rows(i).Item("dblFebruar"),
                                    dtOld.Rows(i).Item("dblMärz"),
                                    dtOld.Rows(i).Item("dblApril"),
                                    dtOld.Rows(i).Item("dblMai"),
                                    dtOld.Rows(i).Item("dblJuni"),
                                    dtOld.Rows(i).Item("dblJuli"),
                                    dtOld.Rows(i).Item("dblAugust"),
                                    dtOld.Rows(i).Item("dblSeptember"),
                                    dtOld.Rows(i).Item("dblOktober"),
                                    dtOld.Rows(i).Item("dblNovember"),
                                    dtOld.Rows(i).Item("dblDezember"),
                                    dtOld.Rows(i).Item("qryKlassifizierung"),
                                    dtOld.Rows(i).Item("qryEntwicklungspaket"),
                                    dtOld.Rows(i).Item("qryPaket"),
                                    dtOld.Rows(i).Item("boolObligo"),
                                    dtOld.Rows(i).Item("txtKst_Lst"),
                                    dtOld.Rows(i).Item("lngSort"),
                                    pbl_dtUser(intCurrentUser).Item("txtPNummer")
                                    )
                                    Next
                                End If
                            End If
                        End If

                        'Versenden von Emails an die Subscriber
                        If Not boolError Then
                            NotifySubscribers() '!ab added
                        End If
                    Catch
                    End Try
#End Region

#Region "Alte Version freezen"
                    Try
                        If cbxFreezeVersion.Checked = True Then
                            FreezeVersion(pbl_IdAp, OldVersion, pbl_Entwurf(pbl_IndexEntwurf).txtEntwurfstyp, True, pbl_VersionAp(pbl_lngVersion).txtVersionErklärung)
                        End If
                    Catch ex As Exception
                    End Try
#End Region

                Catch ex As Exception
                        boolError = True
                    MsgBox(ex.Message.ToString, vbCritical, "Fehler")
                End Try
                'combobox neu und index select
            End If

            Me.Close()

        Catch ex As Exception
            MsgBox(ex.Message.ToString, vbCritical, "Fehler")
        End Try
    End Sub

    Public Sub ComboVersion_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboVersion.SelectedIndexChanged
        Console.WriteLine("EVENT: Public Sub ComboVersion_SelectedIndexChanged")

        Try
            ViewCombo.View_ComboboxVersion(ComboVersion, ComboVersion.SelectedIndex, pbl_VersionAp)
            txtErläuterung.Text = pbl_VersionAp(ComboVersion.SelectedIndex).txtVersionErklärung
        Catch ex As Exception
            MsgBox(ex.Message.ToString, vbCritical, "Fehler")
        End Try
    End Sub

End Class