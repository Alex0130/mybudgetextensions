﻿Imports Microsoft.Office.Tools.Ribbon

Public Class Menueband

    Private Sub cmdStart_Click(sender As Object, e As RibbonControlEventArgs) Handles cmdStart.Click
        Dim i As Integer

        '-- VERBINDUNGEN
        Dim listPossibleCon(1) As String
        Dim strK01 As String
        Dim name = Environment.UserName
        strK01 = "C:\Users\" & name & "\Desktop\dbBudget5.accdb"
        listPossibleCon(0) = strK01 'K01
        listPossibleCon(1) = "X:\Proj\E\E0006\01_EL_Werk\Projektteam\30_Logistik\Budgetplanung\dbBudget5.accdb" 'P01


        Erase listConnections
        For i = LBound(listPossibleCon) To UBound(listPossibleCon)
            Try
                If Check_DB_Connection(listPossibleCon(i)) = True Then 'Verbindung zu dieser Datenbank ist möglich
                    If IsNothing(listConnections) Then
                        ReDim listConnections(0)
                        listConnections(0) = listPossibleCon(i)
                    Else
                        ReDim Preserve listConnections(UBound(listConnections) + 1)
                        listConnections(UBound(listConnections)) = listPossibleCon(i)
                    End If
                End If
            Catch ex As Exception
            End Try
        Next i

        If UBound(listConnections) = 0 Then 'Nur Verbindung zu einem Server möglich
            strPath = listConnections(0)
        Else 'User kann Server auswählen
            Form_SelectConnection.ShowDialog()
        End If
        '--
        If strPath = "" Then Exit Sub

        Try
            Connect_DB() 'Verbindung zur Access Datenbank herstellen
            ThisWorkbook = CType(App.ActiveWorkbook, Excel.Workbook)

            If boolConnected = True Then
                If boolLoggedIn = False Then 'User ist noch nicht angemeldet: Anmeldung erforderlich
                    Get_User(True) 'Liste aller User vom Server laden
                    If Form_Login.comboUser.Items.Count <= 0 Then
                        For i = 0 To UBound(tblUser)
                            Form_Login.comboUser.Items.Add(tblUser(i).txtPNummer)
                        Next
                    End If
                    Form_Login.ShowDialog() 'Login durchführen
                Else
                    'Hauptmenü/Form_Edit laden
                    Try
                        Form_Edit.Show() 'XXX muss irgendwie beim zweiten öffnen geschlossen werden
                    Catch
                        Dim Form_Edit As New EditForm
                        Form_Edit.Show()
                    End Try
                End If
            End If
        Catch
            Exit Sub 'Ohne Daten kein Zugriff
        End Try
        Try
            Form_Edit.BringToFront()
        Catch ex As Exception
        End Try
    End Sub
End Class
