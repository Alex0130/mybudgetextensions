﻿Public Class DatenLinearVerteilenForm
    Private Sub DatenLinearVerteilenForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            comboVonMonat.Items.Clear()
            comboBisMonat.Items.Clear()

            Dim Month() As String
            ReDim Month(11)
            Month(0) = "Januar"
            Month(1) = "Februar"
            Month(2) = "März"
            Month(3) = "April"
            Month(4) = "Mai"
            Month(5) = "Juni"
            Month(6) = "Juli"
            Month(7) = "August"
            Month(8) = "September"
            Month(9) = "Oktober"
            Month(10) = "November"
            Month(11) = "Dezember"

            Dim i As Integer
            For i = 0 To UBound(Month)
                comboVonMonat.Items.Add(Month(i))
                comboBisMonat.Items.Add(Month(i))
            Next

            comboVonMonat.SelectedIndex = 0 'Januar
            comboBisMonat.SelectedIndex = 11 'Dezember

        Catch ex As Exception

        End Try
    End Sub

    Private Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click
        Console.WriteLine("KLICK Private Sub cmdSave_Click")
        Try
            If comboBisMonat.SelectedIndex < comboVonMonat.SelectedIndex Then
                MsgBox("'Bis' Monat muss nach dem 'Von' Monat liegen", vbCritical, "Fehler")
                Exit Sub
            End If

            If Form_Edit.iGrid.Rows.Count = 0 Then
                MsgBox("Keine Daten in der Datentabelle ausgewählt.", vbCritical, "Fehler")
                Exit Sub
            End If

            Dim ZeitSpanne As Integer
            ZeitSpanne = comboBisMonat.SelectedIndex + 1 - comboVonMonat.SelectedIndex

            Dim i As Integer, j As Integer
            Dim Anzahl As Double, SummeProMonat As Double
            For i = 0 To Form_Edit.iGrid.Rows.Count - 1

                Form_Edit.ToolStripStatusOperation.Text = "Anzahl wird linear verteilt: " & i & "/" & Form_Edit.iGrid.Rows.Count - 1
                Form_Edit.StatusStrip.Refresh()

                If Form_Edit.iGrid.Rows(i).Visible = True Then 'Nur aktuell angezeigte Zeilen verändern
                    Anzahl = Form_Edit.iGrid.Rows(i).Cells("AmountPerYear").Value
                    If Not String.IsNullOrEmpty(Anzahl) AndAlso Not Anzahl = 0 Then
                        SummeProMonat = Anzahl / ZeitSpanne

#Region "Anzahl linear verteilen"
                        For j = 1 To 12 'Geht alle Monate pro Zeile durch
                            Dim curMonat As tableMonat = ConvertIntegerToDoubleMonth(j)
                            If j >= comboVonMonat.SelectedIndex + 1 AndAlso j <= comboBisMonat.SelectedIndex + 1 Then 'Monat liegt im Zeitraum: Daten eintragen
                                Form_Edit.iGrid.Rows(i).Cells(curMonat.dblMonat).Value = SummeProMonat
                            Else 'Monat liegt nicht im Zeitraum: Daten nullen
                                Form_Edit.iGrid.Rows(i).Cells(curMonat.dblMonat).Value = 0
                            End If
                            If String.IsNullOrEmpty(Form_Edit.iGrid.Rows(i).Cells("Action").Value) Then
                                Form_Edit.iGrid.Rows(i).Cells("Action").Value = "C"
                            End If
                        Next
#End Region

                    End If
                End If
            Next

            Form_Edit.iGrid_InitializeNumbers() 'Zahlen neu berechnen

            boolDataChanged = True 'Es haben sich Daten geändert

            Form_Edit.ToolStripStatusOperation.Text = "Die Anzahl Werte wurden erfoglreich linear verteilt!"
            Form_Edit.StatusStrip.Refresh()
            Form_Edit.TimerEditForm.Interval = 5000 '1000 = 1 sec
            Form_Edit.TimerEditForm.Start()

            Me.Close()
        Catch ex As Exception
            MsgBox("cmdSave_Click: " & ex.Message.ToString)
        End Try
    End Sub
End Class