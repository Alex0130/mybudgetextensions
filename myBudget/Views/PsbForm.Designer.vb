﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PsbForm
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(PsbForm))
        Me.cmdPsb = New System.Windows.Forms.Button()
        Me.cmdAll = New System.Windows.Forms.Button()
        Me.cmdNothing = New System.Windows.Forms.Button()
        Me.lblGJ = New System.Windows.Forms.Label()
        Me.checkedListEntwurf = New System.Windows.Forms.CheckedListBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.StatusStrip = New System.Windows.Forms.StatusStrip()
        Me.PsbStatusText = New System.Windows.Forms.ToolStripStatusLabel()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.chxGj = New System.Windows.Forms.CheckedListBox()
        Me.cbxEntpivot = New System.Windows.Forms.CheckBox()
        Me.iGridR = New TenTec.Windows.iGridLib.iGrid()
        Me.IGrid1DefaultCellStyle = New TenTec.Windows.iGridLib.iGCellStyle(True)
        Me.IGrid1DefaultColHdrStyle = New TenTec.Windows.iGridLib.iGColHdrStyle(True)
        Me.IGrid1RowTextColCellStyle = New TenTec.Windows.iGridLib.iGCellStyle(True)
        Me.IGAutoFilterManager1 = New TenTec.Windows.iGridLib.Filtering.iGAutoFilterManager()
        Me.cbxExcelExport = New System.Windows.Forms.CheckBox()
        Me.Timer = New System.Windows.Forms.Timer(Me.components)
        Me.UpdateData = New System.Windows.Forms.PictureBox()
        Me.cbxInactiveAp = New System.Windows.Forms.CheckBox()
        Me.cbxGetAllVersions = New System.Windows.Forms.CheckBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ExcelExport = New System.Windows.Forms.PictureBox()
        Me.StatusStrip.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.iGridR, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UpdateData, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ExcelExport, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cmdPsb
        '
        Me.cmdPsb.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdPsb.BackColor = System.Drawing.Color.DarkRed
        Me.cmdPsb.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cmdPsb.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdPsb.ForeColor = System.Drawing.Color.White
        Me.cmdPsb.Location = New System.Drawing.Point(934, 854)
        Me.cmdPsb.Name = "cmdPsb"
        Me.cmdPsb.Size = New System.Drawing.Size(130, 35)
        Me.cmdPsb.TabIndex = 25
        Me.cmdPsb.Text = "Anzeigen"
        Me.cmdPsb.UseVisualStyleBackColor = False
        '
        'cmdAll
        '
        Me.cmdAll.BackColor = System.Drawing.Color.White
        Me.cmdAll.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cmdAll.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdAll.ForeColor = System.Drawing.Color.DarkRed
        Me.cmdAll.Location = New System.Drawing.Point(12, 113)
        Me.cmdAll.Name = "cmdAll"
        Me.cmdAll.Size = New System.Drawing.Size(50, 35)
        Me.cmdAll.TabIndex = 26
        Me.cmdAll.Text = "Alle"
        Me.cmdAll.UseVisualStyleBackColor = False
        '
        'cmdNothing
        '
        Me.cmdNothing.BackColor = System.Drawing.Color.White
        Me.cmdNothing.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cmdNothing.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdNothing.ForeColor = System.Drawing.Color.DarkRed
        Me.cmdNothing.Location = New System.Drawing.Point(68, 113)
        Me.cmdNothing.Name = "cmdNothing"
        Me.cmdNothing.Size = New System.Drawing.Size(50, 35)
        Me.cmdNothing.TabIndex = 27
        Me.cmdNothing.Text = "Keine"
        Me.cmdNothing.UseVisualStyleBackColor = False
        '
        'lblGJ
        '
        Me.lblGJ.AutoSize = True
        Me.lblGJ.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGJ.ForeColor = System.Drawing.Color.DarkRed
        Me.lblGJ.Location = New System.Drawing.Point(12, 9)
        Me.lblGJ.Name = "lblGJ"
        Me.lblGJ.Size = New System.Drawing.Size(103, 16)
        Me.lblGJ.TabIndex = 29
        Me.lblGJ.Text = "Geschäftsjahr"
        '
        'checkedListEntwurf
        '
        Me.checkedListEntwurf.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.checkedListEntwurf.ForeColor = System.Drawing.Color.Black
        Me.checkedListEntwurf.FormattingEnabled = True
        Me.checkedListEntwurf.Location = New System.Drawing.Point(361, 10)
        Me.checkedListEntwurf.Name = "checkedListEntwurf"
        Me.checkedListEntwurf.Size = New System.Drawing.Size(120, 89)
        Me.checkedListEntwurf.TabIndex = 31
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.DarkRed
        Me.Label1.Location = New System.Drawing.Point(269, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(86, 16)
        Me.Label1.TabIndex = 32
        Me.Label1.Text = "Entwurfstyp"
        '
        'StatusStrip
        '
        Me.StatusStrip.BackColor = System.Drawing.SystemColors.ControlLight
        Me.StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PsbStatusText})
        Me.StatusStrip.Location = New System.Drawing.Point(0, 899)
        Me.StatusStrip.Name = "StatusStrip"
        Me.StatusStrip.Size = New System.Drawing.Size(1076, 22)
        Me.StatusStrip.TabIndex = 33
        Me.StatusStrip.Text = "StatusStrip1"
        '
        'PsbStatusText
        '
        Me.PsbStatusText.Name = "PsbStatusText"
        Me.PsbStatusText.Size = New System.Drawing.Size(39, 17)
        Me.PsbStatusText.Text = "Status"
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.DarkRed
        Me.PictureBox1.Location = New System.Drawing.Point(261, 6)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(2, 101)
        Me.PictureBox1.TabIndex = 37
        Me.PictureBox1.TabStop = False
        '
        'PictureBox3
        '
        Me.PictureBox3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox3.BackColor = System.Drawing.Color.DarkRed
        Me.PictureBox3.Location = New System.Drawing.Point(0, 105)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(1200, 2)
        Me.PictureBox3.TabIndex = 38
        Me.PictureBox3.TabStop = False
        '
        'chxGj
        '
        Me.chxGj.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chxGj.FormattingEnabled = True
        Me.chxGj.Location = New System.Drawing.Point(121, 10)
        Me.chxGj.Name = "chxGj"
        Me.chxGj.Size = New System.Drawing.Size(120, 89)
        Me.chxGj.TabIndex = 39
        '
        'cbxEntpivot
        '
        Me.cbxEntpivot.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbxEntpivot.AutoSize = True
        Me.cbxEntpivot.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxEntpivot.ForeColor = System.Drawing.Color.DarkRed
        Me.cbxEntpivot.Location = New System.Drawing.Point(775, 862)
        Me.cbxEntpivot.Name = "cbxEntpivot"
        Me.cbxEntpivot.Size = New System.Drawing.Size(153, 20)
        Me.cbxEntpivot.TabIndex = 40
        Me.cbxEntpivot.Text = "Daten entpivotisieren"
        Me.cbxEntpivot.UseVisualStyleBackColor = True
        '
        'iGridR
        '
        Me.iGridR.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.iGridR.DefaultCol.CellStyle = Me.IGrid1DefaultCellStyle
        Me.iGridR.DefaultCol.ColHdrStyle = Me.IGrid1DefaultColHdrStyle
        Me.iGridR.Location = New System.Drawing.Point(12, 154)
        Me.iGridR.Name = "iGridR"
        Me.iGridR.Size = New System.Drawing.Size(1052, 694)
        Me.iGridR.TabIndex = 44
        '
        'IGAutoFilterManager1
        '
        Me.IGAutoFilterManager1.Grid = Me.iGridR
        '
        'cbxExcelExport
        '
        Me.cbxExcelExport.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbxExcelExport.AutoSize = True
        Me.cbxExcelExport.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxExcelExport.ForeColor = System.Drawing.Color.DarkRed
        Me.cbxExcelExport.Location = New System.Drawing.Point(668, 862)
        Me.cbxExcelExport.Name = "cbxExcelExport"
        Me.cbxExcelExport.Size = New System.Drawing.Size(101, 20)
        Me.cbxExcelExport.TabIndex = 45
        Me.cbxExcelExport.Text = "Excel Export"
        Me.cbxExcelExport.UseVisualStyleBackColor = True
        '
        'Timer
        '
        '
        'UpdateData
        '
        Me.UpdateData.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.UpdateData.Image = CType(resources.GetObject("UpdateData.Image"), System.Drawing.Image)
        Me.UpdateData.Location = New System.Drawing.Point(994, 9)
        Me.UpdateData.Name = "UpdateData"
        Me.UpdateData.Size = New System.Drawing.Size(35, 35)
        Me.UpdateData.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.UpdateData.TabIndex = 47
        Me.UpdateData.TabStop = False
        '
        'cbxInactiveAp
        '
        Me.cbxInactiveAp.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbxInactiveAp.AutoSize = True
        Me.cbxInactiveAp.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxInactiveAp.ForeColor = System.Drawing.Color.Black
        Me.cbxInactiveAp.Location = New System.Drawing.Point(846, 79)
        Me.cbxInactiveAp.Name = "cbxInactiveAp"
        Me.cbxInactiveAp.Size = New System.Drawing.Size(218, 20)
        Me.cbxInactiveAp.TabIndex = 48
        Me.cbxInactiveAp.Text = "Inaktive Arbeitspakete anzeigen"
        Me.cbxInactiveAp.UseVisualStyleBackColor = True
        '
        'cbxGetAllVersions
        '
        Me.cbxGetAllVersions.AutoSize = True
        Me.cbxGetAllVersions.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxGetAllVersions.ForeColor = System.Drawing.Color.DarkRed
        Me.cbxGetAllVersions.Location = New System.Drawing.Point(487, 10)
        Me.cbxGetAllVersions.Name = "cbxGetAllVersions"
        Me.cbxGetAllVersions.Size = New System.Drawing.Size(219, 20)
        Me.cbxGetAllVersions.TabIndex = 49
        Me.cbxGetAllVersions.Text = "Ältere Versionen auswählen"
        Me.cbxGetAllVersions.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(941, 47)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(123, 16)
        Me.Label2.TabIndex = 50
        Me.Label2.Text = "Daten aktualisieren"
        '
        'ExcelExport
        '
        Me.ExcelExport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ExcelExport.Image = CType(resources.GetObject("ExcelExport.Image"), System.Drawing.Image)
        Me.ExcelExport.Location = New System.Drawing.Point(1029, 113)
        Me.ExcelExport.Name = "ExcelExport"
        Me.ExcelExport.Size = New System.Drawing.Size(35, 35)
        Me.ExcelExport.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.ExcelExport.TabIndex = 63
        Me.ExcelExport.TabStop = False
        '
        'PsbForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1076, 921)
        Me.Controls.Add(Me.ExcelExport)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cbxGetAllVersions)
        Me.Controls.Add(Me.cbxInactiveAp)
        Me.Controls.Add(Me.UpdateData)
        Me.Controls.Add(Me.cbxExcelExport)
        Me.Controls.Add(Me.iGridR)
        Me.Controls.Add(Me.cbxEntpivot)
        Me.Controls.Add(Me.chxGj)
        Me.Controls.Add(Me.PictureBox3)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.StatusStrip)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.checkedListEntwurf)
        Me.Controls.Add(Me.lblGJ)
        Me.Controls.Add(Me.cmdNothing)
        Me.Controls.Add(Me.cmdAll)
        Me.Controls.Add(Me.cmdPsb)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "PsbForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Report generieren"
        Me.StatusStrip.ResumeLayout(False)
        Me.StatusStrip.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.iGridR, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UpdateData, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ExcelExport, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cmdPsb As System.Windows.Forms.Button
    Friend WithEvents cmdAll As System.Windows.Forms.Button
    Friend WithEvents cmdNothing As System.Windows.Forms.Button
    Friend WithEvents lblGJ As System.Windows.Forms.Label
    Friend WithEvents checkedListEntwurf As System.Windows.Forms.CheckedListBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents StatusStrip As System.Windows.Forms.StatusStrip
    Friend WithEvents PsbStatusText As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents chxGj As CheckedListBox
    Friend WithEvents cbxEntpivot As System.Windows.Forms.CheckBox
    Friend WithEvents iGridR As TenTec.Windows.iGridLib.iGrid
    Friend WithEvents IGrid1DefaultCellStyle As TenTec.Windows.iGridLib.iGCellStyle
    Friend WithEvents IGrid1DefaultColHdrStyle As TenTec.Windows.iGridLib.iGColHdrStyle
    Friend WithEvents IGrid1RowTextColCellStyle As TenTec.Windows.iGridLib.iGCellStyle
    Friend WithEvents IGAutoFilterManager1 As TenTec.Windows.iGridLib.Filtering.iGAutoFilterManager
    Friend WithEvents cbxExcelExport As System.Windows.Forms.CheckBox
    Friend WithEvents Timer As Timer
    Friend WithEvents UpdateData As PictureBox
    Friend WithEvents cbxInactiveAp As System.Windows.Forms.CheckBox
    Friend WithEvents cbxGetAllVersions As System.Windows.Forms.CheckBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents ExcelExport As PictureBox
End Class
