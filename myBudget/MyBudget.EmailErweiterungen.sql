/* Datenbankänderungen für Email-Versand */
USE MyBudget

Alter Table dbo.tblUser
ADD txtEmail VARCHAR (255);

CREATE TABLE tblUserAP(
 ID INT PRIMARY KEY IDENTITY (1,1),
 qryPNummer NVARCHAR(255) NOT NULL,
 qryID_AP INT NOT NULL,
 qryEntwurfstyp NVARCHAR(255) NOT NULL,
 FOREIGN KEY (qryPNummer) REFERENCES dbo.tblUser (txtPNummer),
 FOREIGN Key (qryID_AP) REFERENCES dbo.tblArbeitspaket (ID_AP),
 FOREIGN Key (qryEntwurfstyp) REFERENCES dbo.tblEntwurfstyp
 );
