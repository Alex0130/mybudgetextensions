﻿Imports System.Windows.Forms
Imports Microsoft.VisualBasic

Namespace UI

    Public Class UIChange

        Function View_ComboboxProjekt(Combobox As ComboBox, SelectedIndex As Integer, dt_Project As System.Data.DataTable) As Integer
            Console.WriteLine("Function View_ComboboxProjekt")

            Combobox.Items.Clear() 'Alte Daten löschen
            Combobox.Text = ""

            Try
                If dt_Project.Rows.Count > 0 Then
                    For i = 0 To dt_Project.Rows.Count - 1 'Daten von DB laden
                        Combobox.Items.Add(dt_Project.Rows(i).Item("txtProjekt"))
                    Next
                End If
                RemoveHandler Form_Edit.ComboProjekt.SelectedIndexChanged, AddressOf Form_Edit.ComboProjekt_SelectedIndexChanged
                RemoveHandler Form_Arbeitspaket.ComboProjekt.SelectedIndexChanged, AddressOf Form_Arbeitspaket.ComboProjekt_SelectedIndexChanged
                RemoveHandler Form_Entwicklungspaket.ComboProjekt.SelectedIndexChanged, AddressOf Form_Entwicklungspaket.ComboProjekt_SelectedIndexChanged
                Combobox.SelectedIndex = SelectedIndex
                View_ComboboxProjekt = Combobox.SelectedIndex
                AddHandler Form_Edit.ComboProjekt.SelectedIndexChanged, AddressOf Form_Edit.ComboProjekt_SelectedIndexChanged
                AddHandler Form_Arbeitspaket.ComboProjekt.SelectedIndexChanged, AddressOf Form_Arbeitspaket.ComboProjekt_SelectedIndexChanged
                AddHandler Form_Entwicklungspaket.ComboProjekt.SelectedIndexChanged, AddressOf Form_Entwicklungspaket.ComboProjekt_SelectedIndexChanged
            Catch
                boolError = True
                View_ComboboxProjekt = -1 'Keine Daten vorhanden
            End Try
        End Function

        Function View_ComboboxEntwurf(Combobox As ComboBox, SelectedIndex As Integer, listEntwurf As tableEntwurfstyp()) As Integer
            Console.WriteLine("Function View_ComboboxEntwurf")

            Combobox.Items.Clear() 'Alte Daten löschen
            Combobox.Text = ""

            Try
                For i = 0 To UBound(listEntwurf)
                    Combobox.Items.Add(listEntwurf(i).ReportBeschreibung)
                Next
                RemoveHandler Form_Edit.ComboEntwurf.SelectedIndexChanged, AddressOf Form_Edit.ComboEntwurf_SelectedIndexChanged
                RemoveHandler Form_Kopieren.ComboEntwurf.SelectedIndexChanged, AddressOf Form_Kopieren.ComboEntwurf_SelectedIndexChanged
                Combobox.SelectedIndex = SelectedIndex
                View_ComboboxEntwurf = Combobox.SelectedIndex
                AddHandler Form_Edit.ComboEntwurf.SelectedIndexChanged, AddressOf Form_Edit.ComboEntwurf_SelectedIndexChanged
                AddHandler Form_Kopieren.ComboEntwurf.SelectedIndexChanged, AddressOf Form_Kopieren.ComboEntwurf_SelectedIndexChanged
            Catch
                boolError = True
                View_ComboboxEntwurf = -1 'Keine Daten vorhanden
            End Try
        End Function

        Function View_ComboboxArbeitspaket(ComboBox As ComboBox, SelectedIndex As Integer, dtAp As System.Data.DataTable) As Integer
            Console.WriteLine("Function View_ComboboxArbeitspaket")

            ComboBox.Items.Clear() 'Alte Daten löschen
            ComboBox.Text = ""

            Try
                If dtAp.Rows.Count > 0 Then
                    For i = 0 To dtAp.Rows.Count - 1
                        ComboBox.Items.Add(dtAp.Rows(i).Item("txtArbeitspaket") & " - " & dtAp.Rows(i).Item("txtPSP_Element"))
                    Next
                End If
                RemoveHandler Form_Edit.ComboArbeitspaket.SelectedIndexChanged, AddressOf Form_Edit.ComboArbeitspaket_SelectedIndexChanged
                RemoveHandler Form_Kopieren.comboAp.SelectedIndexChanged, AddressOf Form_Kopieren.comboAp_SelectedIndexChanged
                RemoveHandler Form_Arbeitspaket.ComboArbeitspaket.SelectedIndexChanged, AddressOf Form_Arbeitspaket.ComboArbeitspaket_SelectedIndexChanged
                ComboBox.SelectedIndex = SelectedIndex
                View_ComboboxArbeitspaket = ComboBox.SelectedIndex
                AddHandler Form_Edit.ComboArbeitspaket.SelectedIndexChanged, AddressOf Form_Edit.ComboArbeitspaket_SelectedIndexChanged
                AddHandler Form_Kopieren.comboAp.SelectedIndexChanged, AddressOf Form_Kopieren.comboAp_SelectedIndexChanged
                AddHandler Form_Arbeitspaket.ComboArbeitspaket.SelectedIndexChanged, AddressOf Form_Arbeitspaket.ComboArbeitspaket_SelectedIndexChanged
            Catch
                boolError = True
                View_ComboboxArbeitspaket = -1 'Keine Daten vorhanden
            End Try
        End Function

        Function View_ComboboxEntwicklungspaket(ComboBox As ComboBox, SelectedIndex As Integer, listEntwicklungspaket As tableEntwicklungspaket()) As Integer
            Console.WriteLine("Function View_ComboboxEntwicklungspaket")

            ComboBox.Items.Clear() 'Alte Daten löschen
            ComboBox.Text = ""

            Try
                For i = 0 To UBound(listEntwicklungspaket)
                    ComboBox.Items.Add(listEntwicklungspaket(i).txtEntwicklungspaket)
                Next
                RemoveHandler Form_Entwicklungspaket.ComboEntwicklungspaket.SelectedIndexChanged, AddressOf Form_Entwicklungspaket.ComboEntwicklungspaket_SelectedIndexChanged
                ComboBox.SelectedIndex = SelectedIndex
                View_ComboboxEntwicklungspaket = ComboBox.SelectedIndex
                AddHandler Form_Entwicklungspaket.ComboEntwicklungspaket.SelectedIndexChanged, AddressOf Form_Entwicklungspaket.ComboEntwicklungspaket_SelectedIndexChanged
            Catch
                boolError = True
                View_ComboboxEntwicklungspaket = -1 'Keine Daten vorhanden
            End Try
        End Function

        Function View_ComboboxVersion(ComboBox As ComboBox, SelectedIndex As Integer, listVersion As tableVersionArbeitspaket()) As Integer
            Console.WriteLine("Function View_ComboboxVersion")

            ComboBox.Items.Clear() 'Alte Daten löschen
            ComboBox.Text = ""

            Try
                For i = 0 To UBound(listVersion)
                    ComboBox.Items.Add(listVersion(i).lngVersion)
                Next
                RemoveHandler Form_Edit.ComboVersion.SelectedIndexChanged, AddressOf Form_Edit.ComboVersion_SelectedIndexChanged
                RemoveHandler Form_Version.ComboVersion.SelectedIndexChanged, AddressOf Form_Version.ComboVersion_SelectedIndexChanged
                ComboBox.SelectedIndex = SelectedIndex
                View_ComboboxVersion = ComboBox.SelectedIndex
                AddHandler Form_Edit.ComboVersion.SelectedIndexChanged, AddressOf Form_Edit.ComboVersion_SelectedIndexChanged
                AddHandler Form_Version.ComboVersion.SelectedIndexChanged, AddressOf Form_Version.ComboVersion_SelectedIndexChanged
            Catch
                boolError = True
                View_ComboboxVersion = -1 'Keine Daten vorhanden
            End Try
        End Function

        Function View_ComboboxTeam(ComboBox As ComboBox, SelectedIndex As Integer, listTeam As tableTeam()) As Integer
            Console.WriteLine("Function View_ComboboxTeam")

            ComboBox.Items.Clear() 'Alte Daten löschen
            ComboBox.Text = ""

            Try
                For i = 0 To UBound(listTeam)
                    ComboBox.Items.Add(listTeam(i).txtTeam)
                Next
                RemoveHandler Form_Arbeitspaket.ComboArbeitspaket.SelectedIndexChanged, AddressOf Form_Arbeitspaket.ComboArbeitspaket_SelectedIndexChanged
                ComboBox.SelectedIndex = SelectedIndex
                View_ComboboxTeam = ComboBox.SelectedIndex
                AddHandler Form_Arbeitspaket.ComboArbeitspaket.SelectedIndexChanged, AddressOf Form_Arbeitspaket.ComboArbeitspaket_SelectedIndexChanged
            Catch
                boolError = True
                View_ComboboxTeam = -1 'Keine Daten vorhanden
            End Try
        End Function

        Function View_ComboboxPaket(Combobox As ComboBox, SelectedIndex As Integer, listPaket As tablePaket()) As Integer
            Console.WriteLine("Function View_ComboboxPaket")

            Combobox.Items.Clear() 'Alte Daten löschen
            Combobox.Text = ""

            Try
                For i = 0 To UBound(listPaket)
                    Combobox.Items.Add(listPaket(i).txtPaket)
                Next
                RemoveHandler Form_Paket.ComboPaket.SelectedIndexChanged, AddressOf Form_Paket.ComboPaket_SelectedIndexChanged
                Combobox.SelectedIndex = SelectedIndex
                View_ComboboxPaket = Combobox.SelectedIndex
                AddHandler Form_Paket.ComboPaket.SelectedIndexChanged, AddressOf Form_Paket.ComboPaket_SelectedIndexChanged
            Catch
                boolError = True
                View_ComboboxPaket = -1 'Keine Daten vorhanden
            End Try
        End Function

    End Class

End Namespace
