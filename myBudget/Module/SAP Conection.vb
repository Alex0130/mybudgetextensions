﻿Module SAP
    Public LogonControl As Object
    Public funccontrol As Object
    Public conn As Object
    Public retct As Boolean

    Public Function SAP_Login(dt_SAPCon As System.Data.DataTable) As Boolean
        Console.WriteLine("FUNC: Sub SAP_Login")

        Dim boolS
        On Error Resume Next

        LogonControl = CreateObject("SAP.LogonControl.1")
        funccontrol = CreateObject("SAP.Functions")
        conn = LogonControl.newConnection

        conn.ApplicationServer = dt_SAPCon.Rows(0).Item("ApplicationServer")
        conn.systemnumber = dt_SAPCon.Rows(0).Item("systemnumber")
        conn.system = dt_SAPCon.Rows(0).Item("system")

        conn.client = "001"
        conn.Language = "DE"

        'SSO
        conn.SNC = True
        conn.SNCName = "p:CN=SAP/_service_snc_p01@EMEA.PORSCHE.BIZ"
        conn.SNCQuality = 3
        conn.User = (Environ$("Username"))
        conn.GroupName = "Public"

        conn.MessageServer = dt_SAPCon.Rows(0).Item("MessageServer")


        conn.usesaplogonini = False

        'Try to login silent
        retct = conn.logon(0, True)

        If retct = False Then
            'fail
            'Try to login with prompt
            If (conn.logon(0, False)) Then
                'success
                Return True
            End If
            'fail
            MsgBox("Es konnte keine SAP Verbidung hergestellt werden", vbCritical, "Fehler")
            Return False
        Else
            'success
            Return True
        End If

    End Function

    Public Function SAP_Logout() As Boolean
        Console.WriteLine("FUNC: SAP_Logout")

        On Error Resume Next
        conn.LogOff
        funccontrol = Nothing
        conn = Nothing
        Return False
    End Function

End Module
