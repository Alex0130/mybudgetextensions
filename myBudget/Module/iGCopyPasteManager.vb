Imports System.Text
Imports TenTec.Windows.iGridLib

Public Class iGCopyPasteManager

#Region " Private fields "
		Private fGrid As iGrid
		Private fCopyColumnHeaders As Boolean = False
		Private WithEvents fContextMenu As New System.Windows.Forms.ContextMenu
		Private WithEvents fMenuItemCut As New System.Windows.Forms.MenuItem
		Private WithEvents fMenuItemCopy As New System.Windows.Forms.MenuItem
		Private WithEvents fMenuItemPaste As New System.Windows.Forms.MenuItem
#End Region

#Region " Constructors "

		Public Sub New(ByVal grid As iGrid)
				Me.New(grid, True, False)
		End Sub

		Public Sub New(ByVal grid As iGrid, ByVal extraScrollBarButtons As Boolean)
				Me.New(grid, extraScrollBarButtons, False)
		End Sub

		Public Sub New(ByVal grid As iGrid, ByVal extraScrollBarButtons As Boolean, ByVal copyColumnHeaders As Boolean)
				fGrid = grid
				fCopyColumnHeaders = copyColumnHeaders

				' Grid events
				AddHandler fGrid.CellMouseUp, AddressOf fGrid_CellMouseUp
				AddHandler fGrid.KeyDown, AddressOf fGrid_KeyDown

				' Copy/paste context menu
				fMenuItemCut.Text = "Cut"
				fMenuItemCopy.Text = "Copy"
				fMenuItemPaste.Text = "Paste"
				fContextMenu.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {fMenuItemCut, fMenuItemCopy, fMenuItemPaste})

				' Extra buttons on the vertical scroll bar
				If extraScrollBarButtons Then
						' Add the "Select all"/"Deselect all" custom buttons
						fGrid.VScrollBar.CustomButtons.AddRange(New iGScrollBarCustomButton() { _
							New iGScrollBarCustomButton(iGScrollBarCustomButtonAlign.Far, iGActions.SelectAllCells, -1, "Select all", True, Nothing), _
							New iGScrollBarCustomButton(iGScrollBarCustomButtonAlign.Far, iGActions.DeselectAllCells, -1, "Deselect all", True, Nothing)})
						' We need to make our extra scroll bar buttons always visible
						fGrid.VScrollBar.Visibility = iGScrollBarVisibility.Always
				End If

		End Sub

#End Region

#Region " Public properties "

		Public Property CopyColumnHeaders() As Boolean
				Get
						Return fCopyColumnHeaders
				End Get
				Set(ByVal value As Boolean)
						fCopyColumnHeaders = value
				End Set
		End Property

#End Region

#Region " Main copy/paste functionality "

		Public Function CanCopyToClipboard() As Boolean

				' Check whether some cells are selected.
				If fGrid.RowMode Then
						Return fGrid.SelectedRows.Count > 0
				Else
						Return fGrid.SelectedCells.Count > 0
				End If

		End Function

		Public Sub CopyToClipboard()
				CopyToClipboardInternal(False)
		End Sub

		Public Sub CutToClipboard()
				CopyToClipboardInternal(True)
		End Sub

		Private Sub CopyToClipboardInternal(ByVal cutCells As Boolean)

				If Not CanCopyToClipboard() Then
						Return
				End If

				Dim myFirstSelectedColOrder As Integer
				Dim myLastSelectedColOrder As Integer

				If fGrid.RowMode Then
						myFirstSelectedColOrder = 0
						myLastSelectedColOrder = fGrid.Cols.Count - 1
				Else
						GetFirstLastSelectedColOrder(myFirstSelectedColOrder, myLastSelectedColOrder)
				End If

				' Create the string to pass to the clipboard.
				' This string represents a block of the cells.
				' Each row of cells is separated with the vbCrLf
				' symbols (new line); each cell in a row is 
				' separated with the vbTab symbol (tabulation).
				' Each row should contain the same number of cells.

				Dim myStringBuilder As StringBuilder = New StringBuilder

				If fCopyColumnHeaders Then
						For myColOrder As Integer = myFirstSelectedColOrder To myLastSelectedColOrder
								Dim myCol As iGCol = fGrid.Cols.FromOrder(myColOrder)
								If myCol.Visible OrElse fGrid.SelectInvisibleCells Then
										myStringBuilder.Append(myCol.Text)
										If myColOrder <> myLastSelectedColOrder Then
												myStringBuilder.Append(vbTab)
										End If
								End If
						Next
						myStringBuilder.Append(vbCrLf)
				End If

				Dim myFirstSelectedRowIndex As Integer
				Dim myLastSelectedRowIndex As Integer

				' The SelectedRows/SelectedCells collections are always sorted by RowIndex/ColIndex,
				' so we can easily to know the first/last selected row (or the row with selected cells):
				If fGrid.RowMode Then
						myFirstSelectedRowIndex = fGrid.SelectedRows(0).Index
						myLastSelectedRowIndex = fGrid.SelectedRows(fGrid.SelectedRows.Count - 1).Index
				Else
						myFirstSelectedRowIndex = fGrid.SelectedCells(0).RowIndex
						myLastSelectedRowIndex = fGrid.SelectedCells(fGrid.SelectedCells.Count - 1).RowIndex
				End If

				For myRowIndex As Integer = myFirstSelectedRowIndex To myLastSelectedRowIndex
						If fGrid.Rows(myRowIndex).Visible OrElse fGrid.SelectInvisibleCells Then
								For myColOrder As Integer = myFirstSelectedColOrder To myLastSelectedColOrder
										Dim myCol As iGCol = fGrid.Cols.FromOrder(myColOrder)
										If myCol.Visible OrElse fGrid.SelectInvisibleCells Then
												Dim myCell As iGCell = fGrid.Cells(myRowIndex, myCol.Index)
												Dim myIsCellSelected As Boolean
												If fGrid.RowMode Then
														myIsCellSelected = fGrid.SelectedRows.Contains(fGrid.Rows(myRowIndex))
												Else
														myIsCellSelected = myCell.Selected
												End If
												If myIsCellSelected Then
														myStringBuilder.Append(myCell.Text)
														If cutCells Then
																myCell.Value = Nothing
														End If
												End If
												If myColOrder <> myLastSelectedColOrder Then
														myStringBuilder.Append(vbTab)
												End If
										End If
								Next
								myStringBuilder.Append(vbCrLf)
						End If
				Next

				' Pass the string to the clipboard.
				Clipboard.SetDataObject(myStringBuilder.ToString, True)

		End Sub

    Public Function CanPasteFromClipboard() As Boolean
        ' Check whether the clipboard contains a compatible object.
        Dim myClipboardObject As IDataObject = Clipboard.GetDataObject
        Return Not (myClipboardObject Is Nothing) AndAlso myClipboardObject.GetDataPresent(GetType(String))

    End Function

    Public Sub PasteFromClipboard()

				If Not CanPasteFromClipboard() Then
						Return
				End If

				If fGrid.Rows.Count = 0 OrElse fGrid.Cols.Count = 0 Then
						Return
				End If

				' Determine the start point to paste the data.
				Dim myStartRowIndex As Integer
				Dim myStartColOrder As Integer
				If Not fGrid.CurCell Is Nothing Then
						myStartRowIndex = fGrid.CurCell.RowIndex
						myStartColOrder = fGrid.CurCell.Col.Order
				Else
						myStartRowIndex = 0
						myStartColOrder = 0
				End If

				' Get the string to paste from the clipboard.
				Dim myClipboardObject As IDataObject = Clipboard.GetDataObject
				Dim myString As String = CStr(myClipboardObject.GetData(GetType(String)))

				If fCopyColumnHeaders Then
						myString = myString.Substring(myString.IndexOf(vbCrLf) + 2)
				End If

				' Deselect all the selected cells/rows.
				If fGrid.RowMode Then
						fGrid.PerformAction(iGActions.DeselectAllRows)
				Else
						fGrid.PerformAction(iGActions.DeselectAllCells)
				End If

				' Fill cells from the clipboard string.
				Dim myCells As String()() = GetCellsFromClipboardString(myString)
				Dim myEndRowIndex As Integer = -1
				Dim myEndColOrder As Integer = -1
				If Not myCells Is Nothing Then

						For myRowIndex As Integer = myStartRowIndex To Math.Min(fGrid.Rows.Count - 1, myCells.Length + myStartRowIndex - 1)
								myEndRowIndex = myRowIndex
								If fGrid.RowMode Then
										fGrid.Rows(myRowIndex).Selected = True
								End If
								For myColOrder As Integer = myStartColOrder To Math.Min(fGrid.Cols.Count - 1, myCells(myRowIndex - myStartRowIndex).Length + myStartColOrder - 1)
										myEndColOrder = myColOrder
										With fGrid.Cells(myRowIndex, fGrid.Cols.FromOrder(myColOrder).Index)
												.Value = myCells(myRowIndex - myStartRowIndex)(myColOrder - myStartColOrder)
												If Not fGrid.RowMode Then
														.Selected = True
												End If
										End With
								Next
						Next

				Else
						fGrid.Cells(myStartRowIndex, fGrid.Cols.FromOrder(myStartColOrder).Index).Value = myString
				End If

				' Trying to display the inserted block of selected cells/rows as much as possible.
				If fGrid.RowMode Then
						If myEndRowIndex >= 0 Then
								fGrid.Rows(myEndRowIndex).EnsureVisible()
						End If
						fGrid.Rows(myStartRowIndex).EnsureVisible()
				Else
						If (myEndRowIndex >= 0) AndAlso (myEndColOrder >= 0) Then
								fGrid.Cells(myEndRowIndex, fGrid.Cols.FromOrder(myEndColOrder).Index).EnsureVisible()
						End If
						fGrid.Cells(myStartRowIndex, fGrid.Cols.FromOrder(myStartColOrder).Index).EnsureVisible()
				End If

		End Sub

		' <summary>
		' Retrieve the cell values from the specified clipboard string.
		' </summary>
		Private Function GetCellsFromClipboardString(ByVal value As String) As String()()

				' Get the rows.
				Dim myRows As String() = value.Split(New Char() {vbLf})

				' Remove vbTab characters from the end of the rows.
				For myIndex As Integer = 0 To myRows.Length - 1
						If myRows(myIndex).Length > 0 Then
								If myRows(myIndex).Chars(myRows(myIndex).Length - 1) = vbCr Then
										myRows(myIndex) = myRows(myIndex).Substring(0, myRows(myIndex).Length - 1)
								End If
						End If
				Next

				' If the last row is empty, remove it.
				Dim myRowCount As Integer = myRows.Length
				If myRows(myRows.Length - 1).Length = 0 Then
						myRowCount = myRowCount - 1
				End If

				' Rtrieve the cell values from each row.
				Dim myCells(myRowCount - 1)() As String
				Dim myColCount As Integer = -1
				For myIndex As Integer = 0 To myRowCount - 1

						If myRows(myIndex).Length = 0 Then
								' If the row is empty, fill up the cells with empty values.

								' If there were no rows before an empty one,
								' return nothing (because it is abnormal case).
								If myColCount < 0 Then
										Return Nothing
								End If

								myCells(myIndex) = New String(myColCount) {}

								For myColIndex As Integer = 0 To myColCount - 1
										myCells(myIndex)(myColIndex) = String.Empty
								Next
						Else
								myCells(myIndex) = myRows(myIndex).Split(vbTab)
						End If

						' Set up the column count and check whether all the rows
						' have the same number of cell.
						If myColCount >= 0 Then
								If Not (myCells(myIndex).Length = myColCount) Then
										Return Nothing
								End If
						Else
								myColCount = myCells(myIndex).Length
						End If

				Next

				Return myCells

		End Function

		' <summary>
		' Returns the first and last column in the range of selected cells.
		' </summary>
		Private Sub GetFirstLastSelectedColOrder(ByRef firstSelectedColOrder As Integer, ByRef lastSelectedColOrder As Integer)

				firstSelectedColOrder = Integer.MaxValue
				lastSelectedColOrder = Integer.MinValue

				For Each myCell As iGCell In fGrid.SelectedCells
						If myCell.Col.Order > lastSelectedColOrder Then
								lastSelectedColOrder = myCell.Col.Order
						End If

						If myCell.Col.Order < firstSelectedColOrder Then
								firstSelectedColOrder = myCell.Col.Order
						End If
				Next

		End Sub

#End Region

#Region "Context Menu"

		Private Sub fMenuItemCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fMenuItemCut.Click
				CutToClipboard()
		End Sub

		Private Sub fMenuItemCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fMenuItemCopy.Click
				CopyToClipboard()
		End Sub

		Private Sub fMenuItemPaste_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fMenuItemPaste.Click
				PasteFromClipboard()
		End Sub

#End Region

#Region " Grid events "

		Private Sub fGrid_CellMouseUp(ByVal sender As System.Object, ByVal e As iGCellMouseUpEventArgs)

        'If e.Button = MouseButtons.Right Then

        '		If fGrid.RowMode Then
        '				If Not fGrid.Rows(e.RowIndex).Selected Then
        '						fGrid.PerformAction(iGActions.DeselectAllRows)
        '						fGrid.Rows(e.RowIndex).Selected = True
        '						fGrid.SetCurCell(e.RowIndex, e.ColIndex)
        '				End If
        '		Else
        '				If Not fGrid.Cells(e.RowIndex, e.ColIndex).Selected Then
        '						fGrid.PerformAction(iGActions.DeselectAllCells)
        '						fGrid.Cells(e.RowIndex, e.ColIndex).Selected = True
        '						fGrid.SetCurCell(e.RowIndex, e.ColIndex)
        '				End If
        '		End If

        '		Dim myCanCopy As Boolean = CanCopyToClipboard()
        '		Dim myCanPaste As Boolean = CanPasteFromClipboard()

        '		If Not myCanCopy AndAlso Not myCanPaste Then
        '				Return
        '		End If

        '		fMenuItemCut.Enabled = myCanCopy
        '		fMenuItemCopy.Enabled = myCanCopy
        '		fMenuItemPaste.Enabled = myCanPaste

        '		fContextMenu.Show(fGrid, e.MousePos)

        'End If

    End Sub

		Private Sub fGrid_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs)

				If e.Modifiers = Keys.Control AndAlso e.KeyCode = Keys.X Then
						CutToClipboard()
				ElseIf e.Modifiers = Keys.Control AndAlso e.KeyCode = Keys.C Then
						CopyToClipboard()
				ElseIf e.Modifiers = Keys.Control AndAlso e.KeyCode = Keys.V Then
						PasteFromClipboard()
				ElseIf e.Modifiers = Keys.Control AndAlso e.KeyCode = Keys.A Then
						fGrid.PerformAction(iGActions.SelectAllCells)
				ElseIf e.Modifiers = Keys.Control AndAlso e.KeyCode = Keys.D Then
						fGrid.PerformAction(iGActions.DeselectAllCells)
				End If

		End Sub

#End Region

End Class
