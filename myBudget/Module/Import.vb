﻿Imports Microsoft.Office.Interop.Excel
Imports Microsoft.Office.Interop

Module Import
#Region "Importiert SAP Excel Berichte (Ist + Obligo)"
    Function Import_EinzelkostenIst() As System.Data.DataTable
        Console.WriteLine("FUNC: Function Import_EinzelkostenIst")

        'Excel Datei importieren und in Excel Tabellenblatt ausgeben
        Dim MyConnection As System.Data.OleDb.OleDbConnection
        Dim dataSet As System.Data.DataSet
        Dim MyCommand As System.Data.OleDb.OleDbDataAdapter
        Dim path As String

        Try
            Dim ofd As New OpenFileDialog
            ofd.InitialDirectory = "C:\Users\" & Environment.UserName & "\Desktop"
            ofd.Filter = "Excel Files (*.xlsx)|*.xlsx"
            ofd.Title = "Excel mit IST Einzeldaten auswählen"
            ofd.ShowDialog()
            path = ofd.FileName
        Catch
        End Try

        If String.IsNullOrEmpty(path) Or path = "False" Then
            boolError = True
            Exit Function
        End If

        Try
            MyConnection = New System.Data.OleDb.OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path + ";Extended Properties=Excel 12.0;")
            MyCommand = New Data.OleDb.OleDbDataAdapter("select * from [Sheet1$] where not Kostenart like '%937%'", MyConnection) 'Schließt die negativen Abrechnungskosten aus

            dataSet = New System.Data.DataSet
            MyCommand.Fill(dataSet)
            Dim dt As System.Data.DataTable = dataSet.Tables(0)
            MyConnection.Close()
            boolError = False
            Return dt
        Catch
            boolError = True
            Exit Function
        End Try
    End Function

    Function Import_EinzelkostenObligo() As System.Data.DataTable
        Console.WriteLine("FUNC: Function Import_EinzelkostenObligo")

        'Excel Datei importieren und in Excel Tabellenblatt ausgeben
        Dim MyConnection As System.Data.OleDb.OleDbConnection
        Dim dataSet As System.Data.DataSet
        Dim MyCommand As System.Data.OleDb.OleDbDataAdapter
        Dim path As String

        Try
            Dim ofd As New OpenFileDialog
            ofd.InitialDirectory = "C:\Users\" & Environment.UserName & "\Desktop"
            ofd.Filter = "Excel Files (*.xlsx)|*.xlsx"
            ofd.Title = "Excel mit OBLIGO Einzeldaten auswählen"
            ofd.ShowDialog()
            path = ofd.FileName
        Catch
        End Try

        If String.IsNullOrEmpty(path) Or path = "False" Then
            boolError = True
            Exit Function
        End If

        Try
            MyConnection = New System.Data.OleDb.OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path + ";Extended Properties=Excel 12.0;")
            MyCommand = New Data.OleDb.OleDbDataAdapter("select * from [Sheet1$]", MyConnection)

            dataSet = New System.Data.DataSet
            MyCommand.Fill(dataSet)
            Dim dt As System.Data.DataTable = dataSet.Tables(0)
            MyConnection.Close()
            boolError = False
            Return dt
        Catch
            boolError = True
            Exit Function
        End Try
    End Function
#End Region

#Region "Convert SAP Colum names to myBudget Data colum names"
    Function ConvertIstToTableSap(dt As System.Data.DataTable) As System.Data.DataTable
        Console.WriteLine("FUNC: Function ConvertIstToTableSap")

        Dim i As Long
        Try

            If boolError = True Then Exit Function
#Region "Columns from myBudget.tblData"
            dt.Columns("Kostenart").ColumnName = "txtKst_Lst"
            dt.Columns("Partner-Kstl").ColumnName = "txtBedarfsnummer" 'Kostenstelle 1
            dt.Columns("Belegdatum").ColumnName = "datErstellungsdatum"
            dt.Columns("Periode").ColumnName = "lngSort" 'Monat
            dt.Columns("Geschäftsjahr").ColumnName = "qryGeschäftsjahr" 'Jahr
#End Region

#Region "SAP specific columns"
            dt.Columns("Kostenartenbeschr#").ColumnName = "Beschreibung_Sachkonto"
            dt.Columns("Partnerobjektbezeichnung").ColumnName = "Leistungsart"
            dt.Columns("Ursprungsobjekt").ColumnName = "Kostenstelle2"
            dt.Columns("Objekt").ColumnName = "Objekt"
            dt.Columns("PSP-Element").ColumnName = "PSP"
            dt.Columns("Objektbezeichnung").ColumnName = "Arbeitspaket"
            dt.Columns("Einkaufsbeleg").ColumnName = "Bestellung"
            dt.Columns("Position").ColumnName = "BestellPosition"
            dt.Columns("Bestelltext").ColumnName = "Bestelltext"
            dt.Columns("Wert/KWähr").ColumnName = "Gesamtwert"
            dt.Columns("Menge erfaßt gesamt").ColumnName = "Menge"
            dt.Columns("Beschreibung des Gegenkontos").ColumnName = "Firma"
            dt.Columns("Material").ColumnName = "Materialnummer"
            dt.Columns("GebuchteMengeneinh").ColumnName = "MengenEinheit"
            dt.Columns("Personalnummer").ColumnName = "PNummer"
            dt.Columns("Belegnummer").ColumnName = "Belegnummer"
            dt.Columns("Buchungsdatum").ColumnName = "Buchungsdatum"
#End Region

#Region "New Columns: Columns from myBudget.tblData"
            dt.Columns.Add(New DataColumn() With {.ColumnName = "IdSap", .DataType = GetType(String), .DefaultValue = ""})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "Bestellart", .DataType = GetType(String), .DefaultValue = "BEST"})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "txtBeschreibung", .DataType = GetType(String), .DefaultValue = ""})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "boolObligo", .DataType = GetType(System.Boolean), .DefaultValue = False})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "curKosten_pro_Einheit", .DataType = GetType(System.Double), .DefaultValue = 0})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "dblJanuar", .DataType = GetType(System.Double), .DefaultValue = 0})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "dblFebruar", .DataType = GetType(System.Double), .DefaultValue = 0})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "dblMärz", .DataType = GetType(System.Double), .DefaultValue = 0})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "dblApril", .DataType = GetType(System.Double), .DefaultValue = 0})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "dblMai", .DataType = GetType(System.Double), .DefaultValue = 0})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "dblJuni", .DataType = GetType(System.Double), .DefaultValue = 0})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "dblJuli", .DataType = GetType(System.Double), .DefaultValue = 0})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "dblAugust", .DataType = GetType(System.Double), .DefaultValue = 0})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "dblSeptember", .DataType = GetType(System.Double), .DefaultValue = 0})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "dblOktober", .DataType = GetType(System.Double), .DefaultValue = 0})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "dblNovember", .DataType = GetType(System.Double), .DefaultValue = 0})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "dblDezember", .DataType = GetType(System.Double), .DefaultValue = 0})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "qryArbeitspaket", .DataType = GetType(System.Int16), .DefaultValue = 0})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "qryKostenart", .DataType = GetType(System.String), .DefaultValue = "sonstige Kosten"})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "qryVersion", .DataType = GetType(System.Int16), .DefaultValue = 1})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "qryKlassifizierung", .DataType = GetType(System.String), .DefaultValue = "Muss"})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "qryEntwurfstyp", .DataType = GetType(System.String), .DefaultValue = "Ist"})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "qryPaket", .DataType = GetType(System.String), .DefaultValue = "none"})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "qryEntwicklungspaket", .DataType = GetType(System.String), .DefaultValue = "none"})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "txtBemerkung", .DataType = GetType(System.String), .DefaultValue = ""})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "txtZusatzfeld", .DataType = GetType(System.String), .DefaultValue = ""})
#End Region

#Region "Sortierung nach Monat aufsteigend"
            Dim dataView As New DataView(dt)
            dataView.Sort = " qryGeschäftsjahr desc, lngSort asc, PSP asc, txtKst_Lst asc"
            dt = dataView.ToTable()
#End Region

            dt = ConvertTotblData(dt)
            Return dt
        Catch ex As Exception
            MsgBox(ex.Message.ToString)
        End Try
    End Function

    Function ConvertObligoToTableSap(dt As System.Data.DataTable) As System.Data.DataTable
        Console.WriteLine("FUNC: Function ConvertObligoToTableSap")

        Try

            If boolError = True Then Exit Function
#Region "Columns from myBudget.tblData"
            dt.Columns("Kostenart").ColumnName = "txtKst_Lst" 'x
            dt.Columns("Belegdatum").ColumnName = "datErstellungsdatum" 'x
            dt.Columns("Periode").ColumnName = "lngSort" 'Monat x
            dt.Columns("Geschäftsjahr").ColumnName = "qryGeschäftsjahr" 'Jahr x
#End Region

#Region "SAP specific columns"
            dt.Columns("Kostenartenbeschr#").ColumnName = "Beschreibung_Sachkonto" 'x
            dt.Columns("Objekt").ColumnName = "Objekt" 'x
            dt.Columns("PSP-Element").ColumnName = "PSP"
            dt.Columns("Nr Referenzbeleg").ColumnName = "Bestellung" 'x
            dt.Columns("Referenz Pos#").ColumnName = "BestellPosition" 'x
            dt.Columns("Bezeichnung").ColumnName = "Bestelltext" 'x
            dt.Columns("Plan/Kwähr").ColumnName = "Gesamtwert" 'x
            dt.Columns("Menge/Plan").ColumnName = "Menge" 'x
            dt.Columns("Material").ColumnName = "Materialnummer" 'x
            dt.Columns("Mengeneinheit").ColumnName = "MengenEinheit" 'x
            dt.Columns("Typ des Referenzbeleges").ColumnName = "Bestellart" 'x
#End Region

#Region "New Columns: Columns from myBudget.tblData"
            dt.Columns.Add(New DataColumn() With {.ColumnName = "Belegnummer", .DataType = GetType(String), .DefaultValue = ""})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "PNummer", .DataType = GetType(String), .DefaultValue = ""})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "Firma", .DataType = GetType(String), .DefaultValue = ""})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "Leistungsart", .DataType = GetType(String), .DefaultValue = ""})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "txtBedarfsnummer", .DataType = GetType(String), .DefaultValue = ""})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "Kostenstelle2", .DataType = GetType(String), .DefaultValue = ""})

            dt.Columns.Add(New DataColumn() With {.ColumnName = "IdSap", .DataType = GetType(String), .DefaultValue = ""})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "txtBeschreibung", .DataType = GetType(String), .DefaultValue = ""})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "boolObligo", .DataType = GetType(System.Boolean), .DefaultValue = True})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "curKosten_pro_Einheit", .DataType = GetType(System.Double), .DefaultValue = 0})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "dblJanuar", .DataType = GetType(System.Double), .DefaultValue = 0})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "dblFebruar", .DataType = GetType(System.Double), .DefaultValue = 0})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "dblMärz", .DataType = GetType(System.Double), .DefaultValue = 0})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "dblApril", .DataType = GetType(System.Double), .DefaultValue = 0})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "dblMai", .DataType = GetType(System.Double), .DefaultValue = 0})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "dblJuni", .DataType = GetType(System.Double), .DefaultValue = 0})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "dblJuli", .DataType = GetType(System.Double), .DefaultValue = 0})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "dblAugust", .DataType = GetType(System.Double), .DefaultValue = 0})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "dblSeptember", .DataType = GetType(System.Double), .DefaultValue = 0})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "dblOktober", .DataType = GetType(System.Double), .DefaultValue = 0})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "dblNovember", .DataType = GetType(System.Double), .DefaultValue = 0})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "dblDezember", .DataType = GetType(System.Double), .DefaultValue = 0})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "qryArbeitspaket", .DataType = GetType(System.Int16), .DefaultValue = 0})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "qryKostenart", .DataType = GetType(System.String), .DefaultValue = "sonstige Kosten"})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "qryVersion", .DataType = GetType(System.Int16), .DefaultValue = 1})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "qryKlassifizierung", .DataType = GetType(System.String), .DefaultValue = "Muss"})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "qryEntwurfstyp", .DataType = GetType(System.String), .DefaultValue = "Ist"})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "qryPaket", .DataType = GetType(System.String), .DefaultValue = "none"})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "qryEntwicklungspaket", .DataType = GetType(System.String), .DefaultValue = "none"})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "txtBemerkung", .DataType = GetType(System.String), .DefaultValue = ""})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "txtZusatzfeld", .DataType = GetType(System.String), .DefaultValue = ""})
#End Region

#Region "Sortierung nach Monat aufsteigend"
            Dim dataView As New DataView(dt)
            dataView.Sort = " qryGeschäftsjahr desc, lngSort asc, PSP asc, txtKst_Lst asc"
            dt = dataView.ToTable()
#End Region

            dt = ConvertTotblData(dt)
            Return dt
        Catch ex As Exception
            MsgBox("ConvertObligoToTableSap: " & ex.Message.ToString)
        End Try
    End Function

    Function ConvertTotblData(dt As System.Data.DataTable) As System.Data.DataTable
        Console.WriteLine("FUNC: Function ConvertTotblData")

        Dim i As Integer
        Try

#Region "Änderung / Berechnung von Werten"
            Dim curMonth As Integer = -1
            Dim Month As String

            Dim curPsp As String = -1
            Dim ID_AP As Integer
            Dim dtAP As New System.Data.DataTable
            dtAP = Get_tblAlleArbeitspakete("ADMIN", pbl_dtAuth, True)

            Dim curSakto As String
            Dim Kostenart As String
            Dim listSakto() As tableSachkonto
            listSakto = Get_Sachkonto(False)

            For i = 0 To dt.Rows.Count - 1

                Dim RowDeleted As Boolean = False
                'Leere Zeilen löschen
                If String.IsNullOrEmpty(dt.Rows(i).Item("txtKst_Lst")) Then
                    dt.Rows(i).Delete()
                    i = i - 1
                    If i = dt.Rows.Count - 1 Then
                        Exit For 'Letzte Zeile erreicht
                    End If
                    RowDeleted = True
                End If

                If RowDeleted = False Then
                    'OBJEKT
                    If String.IsNullOrEmpty(dt.Rows(i).Item("PSP")) Then
                        dt.Rows(i).Item("PSP") = dt.Rows(i).Item("Object")
                    End If

                    'BANF Nr auf richtiges Format bringen
                    If dt.Rows(i).Item("Bestellart") = "BANF" Then
                        Do While Len(dt.Rows(i).Item("Bestellung")) < 10 'BANF Nummer an Suchformat anpassen
                            dt.Rows(i).Item("Bestellung") = "0" & dt.Rows(i).Item("Bestellung")
                        Loop
                    End If

                    'Sap Id
                    If Not String.IsNullOrEmpty(dt.Rows(i).Item("Bestellung")) Then
                        Dim BestellPos As String = dt.Rows(i).Item("BestellPosition")
                        If dt.Rows(i).Item("Bestellart") = "BANF" OrElse dt.Rows(i).Item("Bestellart") = "BEST" Then
                            If Len(BestellPos) < 5 Then
                                Do While Len(BestellPos) < 5 'BANF Nummer an Suchformat anpassen
                                    BestellPos = "0" & BestellPos
                                Loop
                            End If
                        End If
                        dt.Rows(i).Item("IdSap") = dt.Rows(i).Item("Bestellung") & "-" & BestellPos
                    Else
                        dt.Rows(i).Item("IdSap") = dt.Rows(i).Item("Belegnummer")
                    End If

                    'Kostenstelle
                    If String.IsNullOrEmpty(dt.Rows(i).Item("txtBedarfsnummer")) Then 'Wenn es keine Kostenstelle gibt, wird das Bedarfsnummerfeld später durch die SAP Bedarfsnummer der Bestellung gefüllt
                        If Not String.IsNullOrEmpty(dt.Rows(i).Item("Kostenstelle2")) Then 'Ist Kostenstelle zwei gefüllt, wird diese verwendet
                            Dim Kst As String = dt.Rows(i).Item("Kostenstelle2")
                            Dim Cut As Integer = InStr(Kst, "/")
                            Dim Kst_new As String = Kst.Substring(0, Cut - 1)
                            dt.Rows(i).Item("txtBedarfsnummer") = Kst_new
                        End If
                    End If

                    'Kosten pro Einheit und Menge pro Monat
                    If Not curMonth = dt.Rows(i).Item("lngSort") Then
                        Month = ConvertIntegerToDoubleMonth(dt.Rows(i).Item("lngSort")).dblMonat
                    End If
                    dt.Rows(i).Item(Month) = dt.Rows(i).Item("Menge")
                    If Not dt.Rows(i).Item("Menge") = 0 Then
                        dt.Rows(i).Item("curKosten_pro_Einheit") = dt.Rows(i).Item("Gesamtwert") / dt.Rows(i).Item("Menge")
                    ElseIf dt.Rows(i).Item("Gesamtwert") = 0 Then
                        dt.Rows(i).Item("curKosten_pro_Einheit") = 0
                        dt.Rows(i).Item(Month) = 1
                    Else
                        dt.Rows(i).Item("curKosten_pro_Einheit") = dt.Rows(i).Item("Gesamtwert")
                        dt.Rows(i).Item(Month) = 1
                    End If

                    'Dim dtPersonal As New System.Data.DataTable
                    'dtPersonal = Get_tblEM_Personal()
                    ''Beschreibung
                    'dt.Rows(i).Item("txtBeschreibung") = CreateCostDescripition(dt, i, dtPersonal)

                    'Arbeitspaket
                    If Not curPsp = dt.Rows(i).Item("PSP") Then
                        Dim foundRows() As Data.DataRow = dtAP.Select("txtPSP_Element = '" & dt.Rows(i).Item("PSP") & "'")
                        If Not IsNothing(foundRows) AndAlso foundRows.Count > 0 Then 'Es gibt mindestens einen Treffer
                            ID_AP = dtAP.Rows(dtAP.Rows.IndexOf(foundRows(0))).Item("ID_AP")
                            curPsp = dt.Rows(i).Item("PSP")
                        Else
                            ID_AP = 0
                        End If
                    End If
                    dt.Rows(i).Item("qryArbeitspaket") = ID_AP

                    'Kostenart
                    If Not curSakto = dt.Rows(i).Item("txtKst_Lst") Then
                        Dim IndexSakto As Integer = Array.FindIndex(listSakto, Function(f) f.Sakto = dt.Rows(i).Item("txtKst_Lst"))
                        If IndexSakto >= 0 Then
                            Kostenart = listSakto(IndexSakto).qryKostenart
                        Else 'Sachkonto anlegen da noch nicht in Verknüpfungstabelle vorhanden
                            'MsgBox("Zum Sachkonto " & dt.Rows(i).Item("txtKst_Lst") & " wurde keine Kostenart gefunden. Bitte pflegen Sie dieses manuell in der Datenbank nach.", vbCritical, "Sachkonto nicht gefunden")
                            Kostenart = listSakto(UBound(listSakto)).qryKostenart 'Keine zugehörige Kostenart gefunden
                            If String.IsNullOrEmpty(dt.Rows(i).Item("Beschreibung_Sachkonto")) Then
                                dt.Rows(i).Item("Beschreibung_Sachkonto") = "unbekannt"
                            End If
                            '>>
                            Add_Sachkonto(dt.Rows(i).Item("txtKst_Lst"), dt.Rows(i).Item("Beschreibung_Sachkonto"), Kostenart, True)
                            '<<
                            listSakto = Get_Sachkonto(True)
                        End If
                    End If
                    dt.Rows(i).Item("qryKostenart") = Kostenart
                End If
            Next
            Return dt
#End Region

        Catch ex As Exception
            MsgBox("ConvertTotblData, Zeile: " & i & " - " & ex.Message.ToString)
        End Try
    End Function
#End Region

#Region "SAP Beschreibungstext erzeugen"
    Function CreateCostDescripition(dt As System.Data.DataTable, i As Integer, dtPersonal As System.Data.DataTable) As String
        Dim Desc As String

        'Mitarbeiter Namen für EM Personal lesen
        If Not String.IsNullOrEmpty(dt.Rows(i).Item("PNummer")) Then
            Dim foundRows() As Data.DataRow = dtPersonal.Select("Pnummer = '" & dt.Rows(i).Item("PNummer") & "'")
            If Not IsNothing(foundRows) AndAlso foundRows.Count > 0 Then 'Es gibt mindestens einen Treffer
                dt.Rows(i).Item("PNummer") = dt.Rows(i).Item("PNummer") & " (" & foundRows(0).Item("Nachname") & ", " & foundRows(0).Item("Vorname") & " - " & foundRows(0).Item("Abteilung") & ")"
            End If
        End If

#Region "Bestellung & Bestellanforderung"
        If Not String.IsNullOrEmpty(dt.Rows(i).Item("Bestellung")) And dt.Rows(i).Item("Bestelltext") = "Dienstleistung ANÜ" And dt.Rows(i).Item("Bestellart") = "BEST" Then 'ANÜ Kosten 
            Desc = "BEST: " & dt.Rows(i).Item("Bestellung") & " - " & dt.Rows(i).Item("BestellPosition") & " | " & dt.Rows(i).Item("Menge") & " " & dt.Rows(i).Item("MengenEinheit") & " | " & dt.Rows(i).Item("Bestelltext") & " | " & dt.Rows(i).Item("Leistungsart") & " | Beleg: " & dt.Rows(i).Item("Belegnummer") & " in " & dt.Rows(i).Item("lngSort") & "/" & dt.Rows(i).Item("qryGeschäftsjahr") & " über " & dt.Rows(i).Item("Gesamtwert") & "€"

        ElseIf Not String.IsNullOrEmpty(dt.Rows(i).Item("Bestellung")) And String.IsNullOrEmpty(dt.Rows(i).Item("Materialnummer")) And dt.Rows(i).Item("Bestellart") = "BEST" Then 'Best ohne Matnr
            Desc = "BEST:   " & dt.Rows(i).Item("Bestellung") & " - " & dt.Rows(i).Item("BestellPosition") & " | " & dt.Rows(i).Item("Menge") & " " & dt.Rows(i).Item("MengenEinheit") & " | " & dt.Rows(i).Item("Bestelltext") & " | " & dt.Rows(i).Item("Leistungsart") & " | Beleg: " & dt.Rows(i).Item("Belegnummer") & " in " & dt.Rows(i).Item("lngSort") & "/" & dt.Rows(i).Item("qryGeschäftsjahr") & " über " & dt.Rows(i).Item("Gesamtwert") & "€"

        ElseIf Not String.IsNullOrEmpty(dt.Rows(i).Item("Bestellung")) And Not String.IsNullOrEmpty(dt.Rows(i).Item("Materialnummer")) And dt.Rows(i).Item("Bestellart") = "BEST" Then 'Best mit Matnr
            Desc = "BEST: " & dt.Rows(i).Item("Bestellung") & " - " & dt.Rows(i).Item("BestellPosition") & " | " & dt.Rows(i).Item("Menge") & " " & dt.Rows(i).Item("MengenEinheit") & " | " & dt.Rows(i).Item("Materialnummer") & " | " & dt.Rows(i).Item("Bestelltext") & " | " & dt.Rows(i).Item("Leistungsart") & " | Beleg: " & dt.Rows(i).Item("Belegnummer") & " in " & dt.Rows(i).Item("lngSort") & "/" & dt.Rows(i).Item("qryGeschäftsjahr") & " über " & dt.Rows(i).Item("Gesamtwert") & "€"

        ElseIf Not String.IsNullOrEmpty(dt.Rows(i).Item("Bestellung")) And String.IsNullOrEmpty(dt.Rows(i).Item("Materialnummer")) And dt.Rows(i).Item("Bestellart") = "BANF" Then 'Banf ohne Matnr
            Desc = "BANF: " & dt.Rows(i).Item("Bestellung") & " - " & dt.Rows(i).Item("BestellPosition") & " | " & dt.Rows(i).Item("Menge") & " " & dt.Rows(i).Item("MengenEinheit") & " | " & dt.Rows(i).Item("Bestelltext") & " in " & dt.Rows(i).Item("lngSort") & "/" & dt.Rows(i).Item("qryGeschäftsjahr") & " über " & dt.Rows(i).Item("Gesamtwert") & "€"

        ElseIf Not String.IsNullOrEmpty(dt.Rows(i).Item("Bestellung")) And Not String.IsNullOrEmpty(dt.Rows(i).Item("Materialnummer")) And dt.Rows(i).Item("Bestellart") = "BANF" Then 'Banf mit Matnr
            Desc = "BANF: " & dt.Rows(i).Item("Bestellung") & " - " & dt.Rows(i).Item("BestellPosition") & " | " & dt.Rows(i).Item("Menge") & " " & dt.Rows(i).Item("MengenEinheit") & " | " & dt.Rows(i).Item("Materialnummer") & " | " & dt.Rows(i).Item("Bestelltext") & " in " & dt.Rows(i).Item("lngSort") & "/" & dt.Rows(i).Item("qryGeschäftsjahr") & " über " & dt.Rows(i).Item("Gesamtwert") & "€"
#End Region

#Region "Kostenstellen"
        ElseIf Not String.IsNullOrEmpty(dt.Rows(i).Item("txtBedarfsnummer")) And Not String.IsNullOrEmpty(dt.Rows(i).Item("PNummer")) Then 'Kst mit P-Nummer
            Desc = dt.Rows(i).Item("Leistungsart") & " | " & dt.Rows(i).Item("Menge") & " " & dt.Rows(i).Item("MengenEinheit") & " | Mitarbeiter: " & dt.Rows(i).Item("PNummer") & " | Beleg: " & dt.Rows(i).Item("Belegnummer") & " in " & dt.Rows(i).Item("lngSort") & "/" & dt.Rows(i).Item("qryGeschäftsjahr") & " über " & dt.Rows(i).Item("Gesamtwert") & "€"

        ElseIf Not String.IsNullOrEmpty(dt.Rows(i).Item("txtBedarfsnummer")) And String.IsNullOrEmpty(dt.Rows(i).Item("PNummer")) Then 'Kst ohne P-Nummer
            Desc = dt.Rows(i).Item("Leistungsart") & " | " & dt.Rows(i).Item("Menge") & " " & dt.Rows(i).Item("MengenEinheit") & " | Beleg: " & dt.Rows(i).Item("Belegnummer") & " in " & dt.Rows(i).Item("lngSort") & "/" & dt.Rows(i).Item("qryGeschäftsjahr") & " über " & dt.Rows(i).Item("Gesamtwert") & "€"
#End Region

#Region "Aufträge"
        ElseIf Not String.IsNullOrEmpty(dt.Rows(i).Item("Materialnummer")) Then 'Matnr vorhanden
            Desc = dt.Rows(i).Item("Materialnummer") & " | " & dt.Rows(i).Item("Bestelltext") & " | " & dt.Rows(i).Item("Leistungsart") & " | Beleg: " & dt.Rows(i).Item("Belegnummer") & " in " & dt.Rows(i).Item("lngSort") & "/" & dt.Rows(i).Item("qryGeschäftsjahr") & " über " & dt.Rows(i).Item("Gesamtwert") & "€"
#End Region

#Region "Objekte"
        ElseIf Not String.IsNullOrEmpty(dt.Rows(i).Item("Firma")) And Not String.IsNullOrEmpty(dt.Rows(i).Item("MengenEinheit")) Then 'Beschreibung Gegenkonto mit Einheit
            Desc = dt.Rows(i).Item("Firma") & " | " & dt.Rows(i).Item("Menge") & " " & dt.Rows(i).Item("MengenEinheit") & " | Beleg: " & dt.Rows(i).Item("Belegnummer") & " in " & dt.Rows(i).Item("lngSort") & "/" & dt.Rows(i).Item("qryGeschäftsjahr") & " über " & dt.Rows(i).Item("Gesamtwert") & "€"

        ElseIf Not String.IsNullOrEmpty(dt.Rows(i).Item("Firma")) And String.IsNullOrEmpty(dt.Rows(i).Item("MengenEinheit")) Then 'Beschreibung Gegenkonto ohne Einheit
            Desc = dt.Rows(i).Item("Firma") & " | Beleg: " & dt.Rows(i).Item("Belegnummer") & " in " & dt.Rows(i).Item("lngSort") & "/" & dt.Rows(i).Item("qryGeschäftsjahr") & " über " & dt.Rows(i).Item("Gesamtwert") & "€"
#End Region

#Region "Alle Keyfelder sind leer"
        Else
            Desc = "Beleg: " & dt.Rows(i).Item("Belegnummer") & " in " & dt.Rows(i).Item("lngSort") & "/" & dt.Rows(i).Item("qryGeschäftsjahr") & " über " & dt.Rows(i).Item("Gesamtwert") & "€"
#End Region

        End If
        Desc = Desc.Replace("'", "")
        Return Desc
    End Function

    Function CreateCostDescripition_V2(dt As System.Data.DataTable, i As Integer, dtPersonal As System.Data.DataTable) As String
        Dim Desc As String

        'Mitarbeiter Namen für EM Personal lesen
        If Not String.IsNullOrEmpty(dt.Rows(i).Item("PNummer")) Then
            Dim foundRows() As Data.DataRow = dtPersonal.Select("Pnummer = '" & dt.Rows(i).Item("PNummer") & "'")
            If Not IsNothing(foundRows) AndAlso foundRows.Count > 0 Then 'Es gibt mindestens einen Treffer
                dt.Rows(i).Item("PNummer") = dt.Rows(i).Item("PNummer") & " (" & foundRows(0).Item("Nachname") & ", " & foundRows(0).Item("Vorname") & " - " & foundRows(0).Item("Abteilung") & ")"
            End If
        End If

        ' "BEST/BANF:" Bestellung - BestellPosition | "AUF:" Auftrag - Auftragstitel | Menge Mengeneinheit | Materialnummer | Bestelltext | "Firma:" Firma | "Mitarbeiter:" PNummer | Leistungsart | lngsort/qryGeschäftsjahr | "Beleg:" Belegnummer " über " Gesamtwert "€"

        Dim Text As String

        If Not String.IsNullOrEmpty(dt.Rows(i).Item("Bestellung")) Then
            Text = Text & " " & dt.Rows(i).Item("Bestellung") & " - " & dt.Rows(i).Item("BestellPosition") & " |"
        End If

        If Not String.IsNullOrEmpty(dt.Rows(i).Item("Auftrag")) Then
            Text = Text & " AUF: " & dt.Rows(i).Item("Auftrag") & " - " & dt.Rows(i).Item("Auftragstitel") & " |"
        End If

        If Not String.IsNullOrEmpty(dt.Rows(i).Item("Menge")) Then
            If Not String.IsNullOrEmpty(dt.Rows(i).Item("Mengeneinheit")) Then
                Text = Text & " " & dt.Rows(i).Item("Menge") & " " & dt.Rows(i).Item("Mengeneinheit") & " |"
            End If
        End If

        If Not String.IsNullOrEmpty(dt.Rows(i).Item("Materialnummer")) Then
            Text = Text & " " & dt.Rows(i).Item("Materialnummer") & " |"
        End If

        If Not String.IsNullOrEmpty(dt.Rows(i).Item("Bestelltext")) Then
            Text = Text & " TEXT: " & dt.Rows(i).Item("Bestelltext") & " |"
        End If

        If Not String.IsNullOrEmpty(dt.Rows(i).Item("Firma")) Then
            Text = Text & " Firma: " & dt.Rows(i).Item("Firma") & " |"
        End If

        If Not String.IsNullOrEmpty(dt.Rows(i).Item("PNummer")) Then
            If Not dt.Rows(i).Item("PNummer") = "00000000" Then
                Text = Text & " Mitarbeiter: " & dt.Rows(i).Item("PNummer") & " |"
            End If
        End If

            If Not String.IsNullOrEmpty(dt.Rows(i).Item("Leistungsart")) Then
            Text = Text & " " & dt.Rows(i).Item("Leistungsart") & " |"
        End If

        Text = Text & " " & dt.Rows(i).Item("lngsort") & "/" & dt.Rows(i).Item("qryGeschäftsjahr") & " | Beleg: " & dt.Rows(i).Item("Belegnummer") & " über " & dt.Rows(i).Item("Gesamtwert") & "€"

        Text = Text.Replace("'", "")
        Return Text
    End Function
#End Region

#Region "Anü Kosten auf Stunden umlegen"
    Function ConvertFremdPCosts(dt As System.Data.DataTable) As System.Data.DataTable
        Console.WriteLine("FUNC: Function ConvertFremdPCosts")

        Dim SumCosts As Double = 0
        Dim SumHours As Double = 0
        Dim CostsPerUnit As Double
        Dim i As Integer

        Try
            Dim Hours() As System.Data.DataRow = dt.Select("txtKst_Lst like '*923400'")
            Dim Costs() As System.Data.DataRow = dt.Select("txtKst_Lst like '*674200'")

            If Not IsNothing(Hours) AndAlso Hours.Count > 0 Then 'Alle Stunden
                For i = 0 To Hours.Count - 1
                    SumHours = SumHours + Hours(i).Item("Menge")
                Next
            End If

            If Not IsNothing(Costs) AndAlso Costs.Count > 0 Then 'Alle Kosten
                For i = 0 To Costs.Count - 1
                    SumCosts = SumCosts + Costs(i).Item("Gesamtwert")
                Next
            End If

            CostsPerUnit = SumCosts / SumHours

            If Not IsNothing(Hours) AndAlso Hours.Count > 0 Then 'Stundenzeilen um Kosten pro Einheit erweitern
                For i = 0 To Hours.Count - 1
                    Hours(i).Item("curKosten_pro_Einheit") = CostsPerUnit
                    Dim dblMonat As String = ConvertIntegerToDoubleMonth(Hours(i).Item("lngSort")).dblMonat
                    Hours(i).Item("Gesamtwert") = CostsPerUnit * Hours(i).Item(dblMonat)
                Next
            End If

            If Not IsNothing(Costs) And Costs.Count > 0 Then 'Kostenzeilen löschen
                For i = Costs.Count - 1 To 0 Step -1
                    Costs(i).Delete()
                Next
            End If

            Return dt
        Catch ex As Exception
            MsgBox("ConvertFremdPCosts: " & ex.Message.ToString)
            Return dt
        End Try
    End Function

#End Region

End Module
