MyBudget Erweiterung für Email-Notifications

Installationvoraussetzung:

	1. 	Mit dem beiliegenden Script "MyBudget.EmailErweiterungen.sql" die Datenbank "myBudget" erweitern.
		Die Tabelle "tblUser" wird um ein Email-Feld erweitert.
		Die Tabelle "tblUserAp" wird neu erzeugt.Sie enthält die vom Benutzer gewählten Email-Benachrichtigen
		bei Änderung des Arbeitspakets und Entwurfstyp.
	
	2.	Für die Email-Ausgabe muß auf dem Arbeitsrechner Outlook installiert sein.
	
Bedienung:
	
	Der Bediener kann über das Menu Verwaltung/User/Email-Benachrichtigung eine Emailadresse definieren.
	Ist eine Emailadresse definiert, kann der Bediener über eine Email-Ikone im Edit-Formular Benachrichtigungen
	setzen oder löschen.
	
	Löscht der Bediener seine Emailadresse werden auch alle zugehörigen Benachrichtigung gelöscht.
	Wird ein Arbeitspaket gelöscht, weden ebenfalls zugehörige Benachrichtigungen gelöscht.
	
	Konnte bei Programmstart kein Outlook-Objekt erzeugt werden, kann der Bediener trotzdem Benachrichtigungen
	setzen oder löschen. Gesetzte Benachrichtigungen werden in diesem Fall durch eine orange Ikone als "zur Zeit
	nicht ausführbar" gekennzeichnet.
	
	Auslöser für die Email-Ausgabe ist ein erfolgreicher Update des Abeitspaketes.
	
	Alexander Bernatzik 12.19