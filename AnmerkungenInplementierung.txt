Anmerkungen zur Implementierung der myBudget-Extensions

Ich habe mich bemüht den vorhandenen Implementierungsstil einzuhalten.
Alle Änderungen und Erweiterungen sind mit "!ab" gekennzeichnet, damit die Stellen schnell zu finden sind.

Email-Notifications:
Das Handling für die Email-Notifications ist im Readme.txt beschrieben.

Anlegen eines neuen Projekts:
Es wird automatisch ein Arbeitspaket und eine erste Version angelegt.

Skalierung Bildschirmanzeige:
Ich habe getestet, ob sich mit dem TableLayoutPanel die Anzeige bei verschiedenen Bildschirmauflösungen
verbessern läßt. Dabei würden die einzelnen Controls des Filterbereichs im Edit-Form auseinandergezogen,
das Ergebnis sah nicht besser aus. Ich habe daher nur geändert, daß Controlls rechts außen auch bei großer 
Bildschirmauflösung nicht nach innen wandern. 